import { test, expect } from '@playwright/test'
import { loginDavid, saveCoverage, failOnJsError } from './utils'
import { join } from 'path'

test.afterEach(saveCoverage)
test.beforeEach(failOnJsError)

test('Upload recording', async ({ page }) => {
  await loginDavid(page)
  await page.getByRole('link', { name: 'Agenda' }).click()
  await page.getByRole('link', { name: 'Dienst' }).click()
  await page.getByTestId('open-song-1').click()
  await expect(page.getByRole('button', { name: 'Upload opname' })).toBeVisible()
  // Upload recording
  await page.getByRole('button', { name: 'Upload opname' }).click()
  await page.getByLabel('Datum').fill('2020-01-01')
  await page.getByLabel('Omschrijving').fill('Practice session')
  await page.getByLabel('Audio').setInputFiles(join(__dirname, 'test-data', 'test.mp3'))
  await page.getByRole('button', { name: 'Submit' }).click()
  await expect(page.getByText('Practice session')).toBeVisible()
  // Delete recording
  await page.getByRole('button', { name: 'Open menu' }).click()
  await page.getByRole('button', { name: 'Verwijder' }).click()
  await page.getByRole('button', { name: 'Ja', exact: true }).click()
  await expect(page.getByText('Practice session')).not.toBeVisible()
})