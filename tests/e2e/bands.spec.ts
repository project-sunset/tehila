import { test, expect } from '@playwright/test'
import { loginDavid, loginJubal, saveCoverage, failOnJsError } from './utils'
import { join } from 'path'

test.afterEach(saveCoverage)
test.beforeEach(failOnJsError)

test('Repertoire', async ({ page }) => {
  await loginDavid(page)
  await page.getByRole('link', { name: 'Repertoire' }).click()
  await expect(page.getByRole('heading', { name: 'Psalm 1' })).toBeVisible()
  await expect(page.getByRole('heading', { name: 'Unwritten Psalm' })).toBeVisible()
  await expect(page.locator('span').filter({ hasText: 'psalm' })).toBeVisible()
})

test('ICS explanation', async ({ page }) => {
  await loginDavid(page)
  await page.getByRole('link', { name: 'Agenda' }).click()
  await page.getByRole('link', { name: 'Abonneer' }).click()
  await expect(page.locator('#icslink')).toBeVisible()
})

test('New band photo', async ({ page }) => {
  await loginDavid(page)
  await page.getByText('Instellingen', { exact: true }).hover()
  await page.getByRole('link', { name: 'Band/kerk instellingen' }).click()
  await page.getByTestId('band-form').getByText('Choose a file...').click()
  await page.getByLabel('Achtergrond foto (vierkant)').setInputFiles(join(__dirname, 'test-data', 'test.jpg'))
  await page.getByTestId('band-form').getByRole('button', { name: 'Submit' }).click()
  await expect(page.locator('#current-band-picture')).toBeVisible()
})

test('Feedback form', async ({ page }) => {
  await loginDavid(page)
  await page.getByRole('button', { name: 'Feedback' }).click()
  await page.getByLabel('Bericht').fill('Make everything look gold!')
  await page.getByRole('button', { name: 'Versturen' }).click()
  await expect(page.getByText('Je bericht is verstuurd!')).toBeVisible()
  await page.getByRole('button', { name: 'close modal' }).click()
  await saveCoverage({ page })
  await page.reload()
  await expect(page.getByRole('link', { name: 'Berichten' })).toBeVisible()
})

test('New user', async ({ page }) => {
  await page.goto('http://localhost:8080/app/#/')
  await page.getByLabel('Username').fill('Solomon')
  await page.getByRole('button', { name: 'Next' }).click()
  await page.getByLabel('Password').fill('psalm72')
  await page.getByRole('button', { name: 'Login' }).click()
  await expect(page.getByRole('button', { name: 'Start!' })).toBeVisible()
  await page.getByRole('button', { name: 'Start!' }).click()
  await expect(page.getByText('Je zit nog niet in een band,')).toBeVisible()
})

test('Decline invitation', async ({ page }) => {
  await loginDavid(page)
  await page.getByRole('button', { name: 'Nieuw lid uitnodigen' }).click()
  await page.getByLabel('Username').fill('Jubal')
  await page.getByRole('button', { name: 'Uitnodiging versturen' }).click()
  await expect(page.getByText('Jubal (uitgenodigd door David')).toBeVisible()
  await page.getByText('Log out').click()
  await saveCoverage({ page })
  await loginJubal(page)
  await expect(page.getByRole('link', { name: 'Uitnodigingen' })).toBeVisible()
  await page.getByRole('link', { name: 'Uitnodigingen' }).click()
  await expect(page.getByText('David nodigt je uit voor')).toBeVisible()
  await page.getByRole('button', { name: 'Verwijder uitnodiging' }).click()
  await expect(page.getByText('Geen uitnodigingen')).toBeVisible()
})

test('Accept invitation', async ({ page }) => {
  await loginDavid(page)
  await page.getByRole('button', { name: 'Nieuw lid uitnodigen' }).click()
  await page.getByLabel('Username').click()
  await page.getByLabel('Username').fill('Jubal')
  await page.getByRole('button', { name: 'Uitnodiging versturen' }).click()
  await expect(page.getByText('Jubal (uitgenodigd door David')).toBeVisible()
  await page.getByText('Log out').click()
  await saveCoverage({ page })
  await loginJubal(page)
  await expect(page.getByRole('link', { name: 'Uitnodigingen' })).toBeVisible()
  await page.getByRole('link', { name: 'Uitnodigingen' }).click()
  await expect(page.getByText('David nodigt je uit voor')).toBeVisible()
  await page.getByRole('button', { name: 'Accepteer uitnodiging' }).click()
  await expect(page.getByRole('heading', { name: 'Original Psalm Project' })).toBeVisible()
})

test('Delete member', async ({ page }) => {
  await loginDavid(page)
  await page.getByText('Original Psalm Project').first().hover()
  await page.getByText('String boys').click()
  await page.getByTestId('open-member-2').click()
  await page.getByRole('button', { name: 'Verwijder als lid' }).click()
  await page.getByRole('button', { name: 'Ja', exact: true }).click()
  await expect(page.getByRole('heading', { name: 'Jubal' })).not.toBeVisible()
  await expect(page.getByText('Jubal')).not.toBeVisible()
})

test('Stats', async ({ page }) => {
  await loginDavid(page)
  await page.getByRole('link', { name: 'Stats' }).click()
  await expect(page.locator('#app')).toContainText('Nummers op repertoire: 2')
})

test('Download export', async ({ page }) => {
  await loginDavid(page)
  await page.getByText('Instellingen', { exact: true }).hover()
  await page.getByRole('link', { name: 'Band/kerk instellingen' }).click()
  const downloadPromise = page.waitForEvent('download')
  await page.getByRole('link', { name: 'Download' }).click()
  const download = await downloadPromise
  expect(download.suggestedFilename()).toBe('Original Psalm Project_export.zip')
})