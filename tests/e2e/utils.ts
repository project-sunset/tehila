import { Page, expect } from "@playwright/test"
import crypto from 'crypto'
import { writeFileSync } from 'fs'

export async function loginDavid(page: Page) {
  await page.goto('http://localhost:8080/app/#/')
  await page.getByLabel('Username').fill('David')
  await page.getByLabel('Username').press('Enter')
  await page.getByLabel('Password').fill('harp')
  await page.getByRole('button', { name: 'Login' }).click()
  await page.getByRole('button', { name: 'Original Psalm Project' }).click()
}

export async function loginJubal(page: Page) {
  await page.goto('http://localhost:8080/app/#/')
  await page.getByLabel('Username').fill('Jubal')
  await page.getByLabel('Username').press('Enter')
  await page.getByLabel('Password').fill('lyre')
  await page.getByRole('button', { name: 'Login' }).click()
}

declare global {
  interface Window {
    __coverage__: object
  }
}

export async function saveCoverage({ page }: { page: Page }) {
  const filename = crypto.randomUUID()
  try {
    const coverage = await page.evaluate(() => {
      if (!window.__coverage__) {
        console.warn('No coverage!')
        return '{}'
      }
      return JSON.stringify(window.__coverage__)
    })
    writeFileSync(`../.nyc_output/${filename}.json`, coverage)
  } catch (err) {
    console.warn(`Couldn't write coverage: ${err}`)
  }
}

export function failOnJsError({ page }: { page: Page }) {
  page.on('pageerror', exception => {
    console.log(`Uncaught exception: "${exception}"\n${exception.stack}`)
    expect(false, 'Javascript error happened on page!').toBe(true)
  })
}