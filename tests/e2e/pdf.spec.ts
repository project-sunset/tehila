import { test, expect } from '@playwright/test'
import { loginDavid, saveCoverage, failOnJsError } from './utils'
import { join } from 'path'

test.afterEach(saveCoverage)
test.beforeEach(failOnJsError)

test('PDF test', async ({ page }) => {
  await loginDavid(page)
  await page.getByRole('link', { name: 'Agenda' }).click()
  await page.getByRole('link', { name: 'Dienst' }).click()
  await page.getByTestId('open-song-3').click()
  // Upload PDF
  await page.getByRole('button', { name: 'Bladmuziek toevoegen' }).click()
  await page.getByLabel('Variant').fill('Jazzy')
  await page.getByLabel('PDF bestand').setInputFiles(join(__dirname, 'test-data', 'sheet_music.pdf'))
  await page.locator('#upload-pdf').click()
  // Check all pages exist
  await expect(page.getByRole('img', { name: 'Sheet music page 0' })).toBeVisible()
  await expect(page.getByRole('img', { name: 'Sheet music page 1' })).toBeVisible()
  await expect(page.getByRole('img', { name: 'Sheet music page 2' })).toBeVisible()
  // Hook this PDF up to the event
  await page.goBack()
  await page.getByTestId('open-song-3').click()
  await expect(page.getByText('Jazzy')).toBeVisible()
  await page.getByRole('button', { name: 'Koppel aan dit event' }).first().click()
  await expect(page.getByText('Voor dit event: Jazzy')).toBeVisible()
  await page.getByRole('link', { name: 'Open', exact: true }).click()
  await expect(page.getByRole('img', { name: 'Sheet music page 0' })).toBeVisible()
  // Delete PDF music
  await page.getByRole('button', { name: '' }).click()
  await page.getByRole('button', { name: 'Verwijderen', exact: true }).click()
  await page.goBack()
  await expect(page.getByText('Voor dit event: Jazzy')).not.toBeVisible()
})