import { test, expect } from '@playwright/test'
import { loginDavid, saveCoverage, failOnJsError } from './utils'

test.afterEach(saveCoverage)
test.beforeEach(failOnJsError)

test('Band repertoire', async ({ page }) => {
  await loginDavid(page)
  await page.getByText('Instellingen', { exact: true }).hover()
  await page.getByRole('link', { name: 'Tehila homepage' }).click()
  await expect(page.getByText('Een webapp voor kerkbands')).toBeVisible()
  await page.getByRole('link', { name: 'The ancients' }).click()
  await page.locator('article').filter({ hasText: 'Original Psalm Project' }).getByRole('link').click()
  // On band repertoire page
  await expect(page.getByRole('link', { name: 'Login' })).toBeVisible()
  await expect(page.getByTestId('filtered-info')).toContainText('Aantal nummers in repertoire: 2')
  // Expand card
  await page.locator('#song-id-1').getByLabel('Klap lied uit').click()
  await expect(page.locator('#song-id-1')).toContainText('Liedbundels: Psalms 1')
  // Filter on title
  await page.getByLabel('Zoek op titel').fill('Unwritten')
  await expect(page.getByTestId('filtered-info')).toContainText('Aantal nummers in repertoire: 2 (1 verborgen)')
  await page.getByLabel('Zoek op titel').fill('')
  await expect(page.getByTestId('filtered-info')).toContainText('Aantal nummers in repertoire: 2')
  // Filter on tag
  await page.getByLabel('Zoek op tag').selectOption('psalm')
  await expect(page.getByTestId('filtered-info')).toContainText('Aantal nummers in repertoire: 2 (1 verborgen)')
  await page.getByLabel('Reset tag').click()
  await expect(page.getByTestId('filtered-info')).toContainText('Aantal nummers in repertoire: 2')
})

test('Contact forms', async ({ page }) => {
  await loginDavid(page)
  await page.getByText('Instellingen', { exact: true }).hover()
  await page.getByRole('link', { name: 'Tehila homepage' }).click()
  // Homepage contact form
  await page.getByLabel('Naam').fill('Flubber')
  await page.getByLabel('Email').fill('flubber@example.com')
  await page.getByLabel('Bericht').fill('Flubber')
  await page.getByRole('button', { name: 'Versturen' }).click()
  await expect(page.getByText('Je bericht is verstuurd!')).toBeVisible()
  await saveCoverage({ page })
  // Band repertoire contact form
  await page.getByRole('link', { name: 'The ancients' }).click()
  await page.locator('article').filter({ hasText: 'Original Psalm Project' }).getByRole('link').click()
  await page.getByRole('button', { name: 'Feedback' }).click()
  await page.getByLabel('Bericht').fill('Flubber')
  await page.getByLabel('Naam').fill('Flubber')
  await page.getByLabel('Email').fill('flubber@example.com')
  await page.getByRole('button', { name: 'Versturen' }).click()
  await expect(page.getByText('Je bericht is verstuurd!')).toBeVisible()
})

test('Beamer lyrics page', async ({ page }) => {
  await loginDavid(page)
  await page.getByText('Instellingen', { exact: true }).hover()
  // Beamer login
  await page.getByRole('link', { name: 'Beamer Texts' }).click()
  await page.getByLabel('Wachtwoord').fill('abcdefgh')
  await page.getByRole('button', { name: 'Inloggen' }).click()
  // Search by number in collection
  await page.getByLabel('Nummer in bundel').fill('1')
  await page.locator('button[name="collection-search"]').click()
  await expect(page.getByText('Geen lyrics beschikbaar')).toBeVisible()
  // Search by title, multiple matches
  await page.getByLabel('Titel').fill('Psalm')
  await page.locator('button[name="title-search"]').click()
  await expect(page.locator('body')).toContainText('Aantal gevonden: 2')
  // Search by title, single match
  await page.getByLabel('Titel').fill('Garden of Eden')
  await page.locator('button[name="title-search"]').click()
  await expect(page.locator('body')).toContainText('This is the Garden of Eden')
})