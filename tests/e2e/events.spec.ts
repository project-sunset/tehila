import { test, expect } from '@playwright/test'
import { loginDavid, saveCoverage, failOnJsError } from './utils'

test.afterEach(saveCoverage)
test.beforeEach(failOnJsError)

test('CRUD event', async ({ page }) => {
  const today = new Date()
  const today_string = today.toISOString().slice(0, 16)
  await loginDavid(page)
  await page.getByRole('link', { name: 'Agenda' }).click()
  // Create event
  await page.getByRole('button', { name: '+' }).click()
  await page.getByLabel('Datum').fill(today_string)
  await page.getByLabel('Beschrijving').fill('Worship night')
  await page.getByRole('button', { name: 'Submit' }).click()
  await expect(page.locator('#app')).toContainText('DienstWorship night')
  await page.getByRole('link', { name: 'Worship night' }).first().click()
  // Add existing song to event
  await page.getByLabel('toggle edit').locator('span').click()
  await page.getByRole('button', { name: 'Voeg nummer toe' }).click()
  await page.getByLabel('Zoek op titel of nummer uit').click()
  await page.getByLabel('Zoek op titel of nummer uit').fill('garden of eden')
  await page.getByRole('button', { name: 'Voeg toe' }).click()
  await expect(page.getByRole('heading', { name: 'Garden of Eden' })).toBeVisible()
  // Add new song to event
  await page.getByRole('button', { name: 'Voeg nummer toe' }).click()
  await page.getByLabel('Zoek op titel of nummer uit').click()
  await page.getByLabel('Zoek op titel of nummer uit').fill('Tower of Babylon')
  await page.getByRole('button', { name: 'Nieuw lied aanmaken' }).click()
  await page.getByTestId('song-form').getByLabel('Titel').click()
  await page.getByTestId('song-form').getByLabel('Titel').fill('Tower of Babylon')
  await page.getByLabel('Release jaar').click()
  await page.getByLabel('Release jaar').fill('2019')
  await page.getByLabel('Artiest, bundel, collectie').click()
  await page.getByLabel('Artiest, bundel, collectie').fill('A writer')
  await page.getByRole('button', { name: 'Submit' }).click()
  await expect(page.getByRole('heading', { name: 'Tower of Babylon' })).toBeVisible()
  // Reorder songs
  await expect(page.getByText('2Tower of Babylon')).toBeVisible()
  await page.getByTestId('reorder-song-1-up').click()
  await expect(page.getByText('1Tower of Babylon')).toBeVisible()
  // Remove song
  await page.getByTestId('remove-song-2').click()
  await expect(page.getByRole('heading', { name: 'Garden of Eden' })).not.toBeVisible()
})

test('Event sheet music', async ({ page }) => {
  await loginDavid(page)
  await page.getByRole('link', { name: 'Dienst' }).click()
  await page.getByRole('link', { name: 'Open alle bladmuziek' }).click()
  await expect(page.getByRole('link', { name: 'Download als ChordPro' })).toBeVisible()
  await expect(page.getByText('Geen bladmuziek gekoppeld aan').first()).toBeVisible()
})