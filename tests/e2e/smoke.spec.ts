import { test, expect } from '@playwright/test'
import { failOnJsError } from './utils'

test.beforeEach(failOnJsError)

test('Smoke', async ({ page }) => {
  if (process.env.WEBSITE) {
    await page.goto(`https://${process.env.WEBSITE}/`)
  } else {
    await page.goto('http://localhost:8080/')
  }
  // Check static content
  await expect(page.getByRole('heading', { name: 'Tehila', exact: true })).toBeVisible()
  await page.getByRole('link', { name: 'De Oostpoort' }).click()
  await expect(page.getByRole('heading', { name: 'Sonorous' })).toBeVisible()
  await page.getByRole('link', { name: 'Repertoire' }).first().click()
  // Check database connection
  await expect(page.getByText('Adonai', { exact: true })).toBeVisible()
  // Check Vue is working
  await page.getByRole('link', { name: 'Login' }).click()
  await expect(page.getByText('Username')).toBeVisible()
})