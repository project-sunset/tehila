import { test, expect, Page, BrowserContext } from '@playwright/test'
import { loginDavid, saveCoverage, failOnJsError } from './utils'
import { readFileSync } from 'fs'
import { join } from 'path'

test.afterEach(saveCoverage)
test.beforeEach(failOnJsError)

async function compareOpenLyrics(filename: string, page: Page, context: BrowserContext) {
  const match = /\d+$/.exec(page.url())
  if (match === null) {
    throw Error(`No match: ${page.url()}`)
  }
  const openLyricsId = match[0]
  const response = await context.request.get(`${process.env.BACKEND_URL}/api/open-lyrics/${openLyricsId}/`, { headers: { 'x-tehila-band': '1' } })
  const openlyricsXml = (await response.json())['content']
  const expectedXml = readFileSync(join(__dirname, 'test-data', `${filename}.xml`), 'utf8').replace(/\n */g, '').replaceAll(' />', '/>')
  expect(openlyricsXml).toEqual(expectedXml)
}

test('Basic song creation', async ({ page, context }) => {
  await loginDavid(page)
  await page.getByRole('link', { name: 'Dienst' }).click()
  await page.getByTestId('open-song-3').click()
  await page.getByRole('button', { name: 'Bladmuziek toevoegen' }).click()
  await page.getByRole('link', { name: 'Open Tehila editor' }).click()
  // Fill metadata
  await page.getByPlaceholder('variant').click()
  await page.getByPlaceholder('variant').fill('origineel')
  await page.locator('#bpm').click()
  await page.locator('#bpm').fill('90')
  await page.locator('#time-signature').selectOption('4/4')
  await page.locator('#add-section-inline').click()
  await page.locator('#section-0-name').fill('vers 1')
  // Create a section
  await page.getByRole('button', { name: 'Maak nieuw vers: vers' }).click()
  await page.locator('[id="\\30 -0-0"]').fill('This is a new psalm')
  await page.locator('[id="\\30 -0-0"]').press('ArrowUp')
  await page.locator('[id="\\30 -0-0-chord"]').fill('D')
  await page.locator('#section-0').getByRole('combobox').selectOption('f')
  await page.locator('#section-0 textarea').click()
  await page.locator('#section-0 textarea').fill('Play well')
  // Save and view
  await page.getByRole('button', { name: '' }).click()
  await page.getByTitle('Stop bewerken').click()
  await expect(page.getByRole('heading', { name: 'Viewer' })).toBeVisible()
  await compareOpenLyrics('basic_song_creation', page, context)
})

test('Public editor MusicXML import', async ({ page }) => {
  await loginDavid(page)
  await page.goto('http://localhost:8080/app/#/public/openlyrics')
  await page.getByText('Choose a file...').click()
  await page.getByLabel('MusicXML file').setInputFiles(join(__dirname, 'test-data', 'test_musicxml.mxl'))
  await page.getByRole('button', { name: 'Upload' }).click()
  await expect(page.getByRole('link', { name: 'This is a test' })).toBeVisible()
})

test('Download ChordPro file', async ({ page }) => {
  await loginDavid(page)
  await page.goto('http://localhost:8080/app/#/openlyrics/1/1')
  await page.getByRole('button', { name: '' }).click()
  const downloadPromise = page.waitForEvent('download')
  await page.getByRole('link', { name: 'Opslaan als ChordPro' }).click()
  const download = await downloadPromise
  await download.saveAs('test.chordpro')
  const data = readFileSync('test.chordpro').toString()
  expect(data).toEqual(`{title: Psalm 1}

{start_of_verse: vers 1}
[Bbm7]Praise, my [Ab/C]soul, the [Db]King of heaven.
[Gb/Bb]I will [Ab/C]listen [Db/F]to Your [Gb]voice.
[Bbm7]Where You [Ab/C]lead me, [Db]I will follow,
[Gb2]in You I [Absus]will re[Db]joice.

{end_of_verse}

{start_of_verse: refrein}
[Ab/Gb]Here [Db/Gb]I [Ebm7]am, [Ab/Gb]here [Db/Gb]I [Db]stand.
[Ab/Gb]By [Db/Gb]Your [Ebm7]promise, this I [Absus]know.[Ab]None
[Ab/Gb]Here [Db/Gb]I [Ebm7]am, [Ab/Gb]in [Db/Gb]Your [Db]hand.
[Ab/Gb]Lord, [Db/Gb]no [Ebm7]matter where I [Absus]go,[Ab]None
[Db]me.[Ab/C]None[Gb/Bb]None[Db/Ab]None[Gb2]None[Absus]None[Db]None
{end_of_verse}

`)
})

test('Public editor', async ({ page }) => {
  await page.goto('http://localhost:8080/app/#/public/openlyrics')
  await page.getByLabel('Title').fill('New Song')
  await page.getByRole('button', { name: 'Add' }).click()
  await expect(page.getByRole('link', { name: 'New Song' })).toBeVisible()
  await page.getByRole('link', { name: 'New Song' }).click()
  await page.getByPlaceholder('variant').fill('origineel')
  await page.getByRole('button', { name: '' }).click()
  await page.getByRole('button', { name: '' }).click()
  await expect(page.getByRole('heading', { name: 'Viewer' })).toBeVisible()
})