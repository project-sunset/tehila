import { test, expect } from '@playwright/test'
import { loginDavid, loginJubal, saveCoverage, failOnJsError } from './utils'

test.afterEach(saveCoverage)
test.beforeEach(failOnJsError)

test('CRUD songs & collections', async ({ page }) => {
  await loginDavid(page)
  await page.getByText('Instellingen', { exact: true }).hover()
  await page.getByRole('link', { name: 'Songs' }).click()
  // Create song
  await page.getByRole('button', { name: 'Nieuw lied aanmaken' }).click()
  await page.getByLabel('Titel', { exact: true }).fill('Psalm 3')
  await page.getByLabel('Release jaar').fill('2019')
  await page.getByLabel('Artiest, bundel, collectie').fill('New artist')
  await page.getByRole('button', { name: 'Submit' }).click()
  await expect(page.getByRole('cell', { name: 'Psalm 3' })).toBeVisible()
  // Update song
  await page.getByRole('button', { name: 'Edit' }).click()
  await page.getByTestId('song-form').getByLabel('Title').fill('Psalm 4')
  await page.getByRole('button', { name: 'Submit' }).click()
  await page.getByLabel('Zoek op titel of nummer uit').fill('Psalm')
  await expect(page.getByRole('cell', { name: 'Psalm 4' })).toBeVisible()
  // Delete song
  await page.getByRole('row', { name: 'Psalm 4 Edit' }).getByRole('button').click()
  await page.getByRole('button', { name: 'Delete' }).click()
  await page.getByLabel('Zoek op titel of nummer uit').fill('Psalm 4')
  await expect(page.getByRole('cell', { name: 'Psalm 4' })).not.toBeVisible()
  // Create collection
  await page.getByRole('button', { name: 'New Collections' }).click()
  await page.getByTestId('collections-form').getByLabel('Title').fill('SB2')
  await page.getByRole('button', { name: 'Submit' }).click()
  await expect(page.getByRole('cell', { name: 'SB2' })).toBeVisible()
  // Update collection
  await page.getByTestId('collections-2-update').click()
  await page.getByTestId('collections-form').getByLabel('Title').fill('SB3')
  await page.getByRole('button', { name: 'Submit' }).click()
  await expect(page.getByRole('cell', { name: 'SB3' })).toBeVisible()
  // Delete collection
  await page.getByTestId('collections-2-update').click()
  await page.getByRole('button', { name: 'Delete' }).click()
  await expect(page.getByRole('cell', { name: 'SB3' })).not.toBeVisible()
})

test('CRUD tags', async ({ page }) => {
  await loginDavid(page)
  await page.getByText('Instellingen', { exact: true }).hover()
  await page.getByRole('link', { name: 'Tags' }).click()
  // Create tag
  await page.getByRole('button', { name: 'New Tag' }).click()
  await page.getByLabel('Naam').fill('groovy')
  await page.getByLabel('Achtergrondkleur').fill('gold')
  await page.getByLabel('Tekstkleur').fill('white')
  await page.getByRole('button', { name: 'Submit' }).click()
  await expect(page.getByRole('cell', { name: 'groovy' }).first()).toBeVisible()
  // Update tag
  await page.getByTestId('tag-3-update').click()
  await page.getByLabel('Naam').fill('upbeat')
  await page.getByRole('button', { name: 'Submit' }).click()
  await expect(page.getByRole('cell', { name: 'upbeat' }).first()).toBeVisible()
  // Delete tag
  await page.getByTestId('tag-3-update').click()
  await page.getByRole('button', { name: 'Delete' }).click()
  await expect(page.getByRole('cell', { name: 'upbeat' }).first()).not.toBeVisible()
  // Add tag to song
  await page.getByRole('link', { name: 'Repertoire' }).click()
  await page.getByTestId('open-song-3').click()
  await page.getByLabel('toggle edit').click()
  await page.getByText('Add').nth(1).click()
  await page.getByRole('button', { name: 'Submit' }).click()
  await expect(page.getByTestId('song-form')).not.toBeVisible()
  await expect(page.locator('span').filter({ hasText: 'legendary' })).toBeVisible()
  // Remove tag from song
  await page.getByLabel('toggle edit').click()
  await page.getByTestId('delete-tag-2').click()
  await page.getByRole('button', { name: 'Submit' }).click()
  await expect(page.getByTestId('song-form')).not.toBeVisible()
  await expect(page.locator('span').filter({ hasText: 'legendary' })).not.toBeVisible()
})

test('View song lyrics', async ({ page }) => {
  await loginJubal(page)
  await page.getByRole('link', { name: 'Repertoire' }).click()
  await page.getByTestId('open-song-2').click()
  await page.getByRole('button', { name: 'Show' }).click()
  await expect(page.getByText('This is the Garden of').first()).toBeVisible()
})