#! /bin/bash
set -e
cd ..
mkdir -p .nyc_output
source .venv/bin/activate
cd backend
./refresh_database.sh nowebauthn
python3 manage.py runserver --noreload