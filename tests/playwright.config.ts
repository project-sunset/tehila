import { defineConfig, devices } from '@playwright/test'
import { readFileSync } from 'fs'

/**
 * See https://playwright.dev/docs/test-configuration.
 */
const WEB_SERVER = [{
  command: './startFrontend.sh',
  url: 'http://localhost:8080',
  reuseExistingServer: false,
}, {
  command: './startBackend.sh',
  url: 'http://localhost:8000',
  reuseExistingServer: false,
}]

process.env.BACKEND_URL = 'http://localhost:8000'
if (process.env.NO_WEBSERVER && !process.env.SMOKE_TEST) {
  process.env.BACKEND_URL = JSON.parse(readFileSync('config.json').toString()).server_url
}

export default defineConfig({
  testDir: './e2e',
  fullyParallel: false,
  forbidOnly: !!process.env.CI,
  retries: 0,
  workers: 1,
  reporter: process.env.CI ? 'dot' : 'html',
  use: {
    trace: 'on-first-retry',
    screenshot: {
      mode: 'only-on-failure',
      fullPage: true
    }
  },
  projects: [
    {
      name: 'songs',
      use: { ...devices['Desktop Chrome'] },
      testMatch: /songs.spec.ts/
    },
    {
      name: 'bands',
      use: { ...devices['Desktop Chrome'] },
      testMatch: /bands.spec.ts/
    },
    {
      name: 'events',
      use: { ...devices['Desktop Chrome'] },
      testMatch: /events.spec.ts/
    },
    {
      name: 'recordings',
      use: { ...devices['Desktop Chrome'] },
      testMatch: /recordings.spec.ts/
    },
    {
      name: 'openlyrics',
      use: { ...devices['Desktop Chrome'] },
      testMatch: /openlyrics.spec.ts/
    },
    {
      name: 'sites',
      use: { ...devices['Desktop Chrome'] },
      testMatch: /sites.spec.ts/
    },
    {
      name: 'pdf',
      use: { ...devices['Desktop Chrome'] },
      testMatch: /pdf.spec.ts/
    },
    {
      name: 'smoke-test',
      use: { ...devices['Desktop Chrome'] },
      testMatch: /smoke.spec.ts/
    },
  ],
  webServer: process.env.NO_WEBSERVER ? undefined : WEB_SERVER
})
