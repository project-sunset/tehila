import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { VitePWA } from 'vite-plugin-pwa'
import path from 'path'
import IstanbulPlugin from 'vite-plugin-istanbul'

const addBuildTimestamp = () => {
  return {
    name: 'add-build-timestamp',
    transformIndexHtml() {
      return {
        tags: [{
          tag: 'meta',
          attrs: {
            'name': 'buildtimestamp',
            'content': Date.now()
          }
        }]
      }
    }
  }
}

// The environment variable is only defined by the PlaywrighTests class in Django.
// For within Vue it should be loaded in automatically from the .env file. 
// But that hasn't happened yet in this config file. Technically this overrides the .env.development file.
if (process.env.NODE_ENV === 'development' && process.env["VITE_APP_API_URL"] === undefined) {
  process.env["VITE_APP_API_URL"] = "http://localhost:8000/"
}

// https://vitejs.dev/config/
export default defineConfig({
  base: '/app/',
  build: {
    assetsDir: 'static',
    sourcemap: true,
  },
  server: {
    hmr: process.env["DISABLE_HMR"] !== "true",
    port: 8080,
    proxy: {
      '/media': process.env["VITE_APP_API_URL"],
      '/recorder': process.env["VITE_APP_API_URL"],
      '/admin': process.env["VITE_APP_API_URL"],
      '/static/admin': process.env["VITE_APP_API_URL"],
      '/docs': process.env["VITE_APP_API_URL"],
      '/disclaimer': process.env["VITE_APP_API_URL"],
      '/api': process.env["VITE_APP_API_URL"]
    }
  },
  plugins: [
    vue(),
    addBuildTimestamp(),
    VitePWA({
      workbox: {
        navigateFallback: null
      },
      includeAssets: ['static/img/favicon.ico', 'static/robots.txt', 'static/img/apple-touch-icon.png'],
      filename: 'service-worker.js',
      manifest: {
        name: 'Tehila',
        short_name: 'Tehila',
        description: 'Tehila',
        theme_color: '#460076',
        icons: [
          {
            src: "./static/img/favicon/android-chrome-512x512.png",
            sizes: "512x512",
            type: "image/png"
          },
          {
            src: "./static/img/favicon/android-chrome-192x192.png",
            sizes: "192x192",
            type: "image/png"
          },
          {
            src: 'static/img/favicon/favicon-32x32.png',
            sizes: '32x32',
            type: 'image/png',
          },
          {
            src: 'static/img/favicon/favicon-16x16.png',
            sizes: '16x16',
            type: 'image/png',
          }
        ]
      }
    }),
    IstanbulPlugin({
      include: "src/*",
      extension: [".js", ".ts", ".vue"],
      requireEnv: true
    })],
  resolve: {
    extensions: ['.mjs', '.js', '.ts', '.jsx', '.tsx', '.json', '.vue'],
    alias: {
      "@": path.resolve(__dirname, "./src"),
    },
  },
})
