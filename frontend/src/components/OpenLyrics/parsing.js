import chords from './chords'

function nsResolver(prefix) {
  return "http://openlyrics.info/namespace/2009/song"
}

function cleanLyrics(text) {
  return text.replace('\n', '')
}

export default {
  getTitle(openlyrics) {
    return this.xpathStr('/ol:song/ol:properties/ol:titles/ol:title[1]', openlyrics)
  },
  getBpm(openlyrics) {
    return this.xpathStr('/ol:song/ol:properties/ol:tempo', openlyrics)
  },
  getTimeSignature(openlyrics) {
    return this.xpathStr('/ol:song/ol:properties/ol:timeSignature', openlyrics)
  },
  getVerseOrder(openlyrics) {
    return this.xpathStr('/ol:song/ol:properties/ol:verseOrder', openlyrics).split(' ')
  },
  getVariant(openlyrics) {
    return this.xpathStr('/ol:song/ol:properties/ol:variant', openlyrics)
  },
  createSections(openlyrics) {
    const sectionNodes = this.xpathNodes('/ol:song/ol:directions/ol:section', openlyrics)
    if (!sectionNodes) return
    const sections = []
    let currentNode
    while ((currentNode = sectionNodes.iterateNext()) !== null) {
      sections.push({
        name: currentNode.getAttribute('verse'),
        dynamic: currentNode.hasAttribute('dynamic') ? currentNode.getAttribute('dynamic') : undefined,
        directions: this.createDirections(currentNode.childNodes),
      })
    }
    return sections
  },
  createVerses(openlyrics) {
    const verseNodes = this.xpathNodes('/ol:song/ol:lyrics/ol:*', openlyrics)
    if (!verseNodes) return
    const verses = {}
    let currentNode
    while ((currentNode = verseNodes.iterateNext()) !== null) {
      if (currentNode.hasChildNodes()) {
        verses[currentNode.getAttribute("name")] = this.createVerse(currentNode)
      } else {
        verses[currentNode.getAttribute("name")] = null
      }
    }
    return verses
  },
  xpathStr(expression, openlyrics) {
    const result = document.evaluate(expression, openlyrics, nsResolver, XPathResult.STRING_TYPE, null)
    return result.stringValue
  },
  xpathNodes(expression, openlyrics) {
    const result = document.evaluate(expression, openlyrics, nsResolver, XPathResult.ORDERED_NODE_ITERATOR_TYPE, null)
    return result
  },
  createDirections(nodes) {
    let text = ""
    nodes.forEach(node => {
      if (node.nodeType === Node.TEXT_NODE) {
        text = text + node.textContent.trim()
      } else if (node.localName === 'br') {
        text = text + '\n'
      }
    })
    return text
  },
  createVerse(verseNode) {
    let verse = []
    let currentLine = []
    verse.push(currentLine)
    verseNode.childNodes.forEach(lines => {
      lines.childNodes.forEach(node => {
        if (node.localName === 'br') {
          currentLine = []
          verse.push(currentLine)
        } else if (node.nodeType === Node.TEXT_NODE) {
          if (node.textContent.trim() !== "") {
            currentLine.push({ text: node.textContent })
          }
        } else if (node.localName === 'chord') {
          currentLine.push({
            text: node.textContent,
            chord: this.createChordText(node),
            newbar: node.hasAttribute("newbar")
          })
        } else {
          console.error('Unknown node!', node)
        }
      })
      currentLine = []
      verse.push(currentLine)
    })
    verse = verse.filter(line => line.length > 0)
    if (verse.length === 0) {
      verse.push([{ text: '' }])
    }
    return verse
  },
  createChordText(node) {
    let text = node.getAttribute('root')
    if (node.hasAttribute('structure')) {
      text = text + node.getAttribute('structure')
    }
    if (node.hasAttribute('bass')) {
      text = text + '/' + node.getAttribute('bass')
    }
    return text
  },
  save(metadata, sections, verses) {
    const ns = "http://openlyrics.info/namespace/2009/song"
    const xmlDoc = document.implementation.createDocument(ns, "song")
    xmlDoc.documentElement.setAttribute("version", "0.9")
    xmlDoc.documentElement.setAttribute("createdIn", "Tehila")
    xmlDoc.documentElement.setAttribute("modifiedIn", "Tehila")
    const properties = xmlDoc.createElementNS(ns, "properties")
    xmlDoc.documentElement.append(properties)
    const titles = xmlDoc.createElementNS(ns, "titles")
    properties.append(titles)
    const title = xmlDoc.createElementNS(ns, "title")
    titles.append(title)
    title.append(xmlDoc.createTextNode(metadata.title))
    if (metadata.bpm) {
      const tempo = document.createElementNS(ns, "tempo")
      tempo.setAttribute("type", "bpm")
      tempo.append(xmlDoc.createTextNode(metadata.bpm))
      properties.append(tempo)
    }
    if (metadata.timeSignature) {
      const timeSignature = document.createElementNS(ns, "timeSignature")
      timeSignature.append(xmlDoc.createTextNode(metadata.timeSignature))
      properties.append(timeSignature)
    }
    if (metadata.variant) {
      const variant = document.createElementNS(ns, "variant")
      variant.append(xmlDoc.createTextNode(metadata.variant))
      properties.append(variant)
    }
    const directions = xmlDoc.createElementNS(ns, "directions")
    xmlDoc.documentElement.append(directions)
    for (let section of sections) {
      const sectionNode = xmlDoc.createElementNS(ns, "section")
      directions.append(sectionNode)
      sectionNode.setAttribute("verse", section.name)
      if (section.dynamic) {
        sectionNode.setAttribute("dynamic", section.dynamic)
      }
      const sectionDirections = section.directions.split('\n')
      for (let text of sectionDirections) {
        sectionNode.append(xmlDoc.createTextNode(text))
        sectionNode.append(xmlDoc.createElementNS(ns, 'br'))
      }
      sectionNode.removeChild(sectionNode.lastElementChild)
    }
    const lyrics = xmlDoc.createElementNS(ns, "lyrics")
    xmlDoc.documentElement.append(lyrics)
    const usedVerses = new Set(sections.map(s => s.name))
    for (let verseKey of usedVerses) {
      const verseNode = xmlDoc.createElementNS(ns, "verse")
      lyrics.append(verseNode)
      verseNode.setAttribute("name", verseKey)
      const lines = xmlDoc.createElementNS(ns, "lines")
      verseNode.append(lines)
      for (let line of verses[verseKey]) {
        for (let part of line) {
          if (!part.chord && !part.newbar) {
            lines.append(xmlDoc.createTextNode(cleanLyrics(part.text)))
          } else if (!part.chord && part.newbar) {
            const chord = xmlDoc.createElementNS(ns, "chord")
            lines.append(chord)
            chord.setAttribute('newbar', 'true')
            chord.append(xmlDoc.createTextNode(cleanLyrics(part.text)))
          } else {
            const chord = xmlDoc.createElementNS(ns, "chord")
            lines.append(chord)
            if (!chords.chordRegex.test(part.chord)) {
              chord.setAttribute('root', part.chord)
            } else {
              const chordMatch = chords.parseChord(part.chord)
              chord.setAttribute('root', chordMatch.root)
              if (chordMatch.structure) {
                chord.setAttribute('structure', chordMatch.structure)
              }
              if (chordMatch.bass) {
                chord.setAttribute('bass', chordMatch.bass)
              }
            }
            if (part.newbar) {
              chord.setAttribute('newbar', 'true')
            }
            if (part.text) {
              chord.append(xmlDoc.createTextNode(cleanLyrics(part.text)))
            }
          }
        }
        lines.append(xmlDoc.createElementNS(ns, "br"))
      }
      if (lines.lastElementChild !== null) {
        lines.removeChild(lines.lastElementChild)
      }
    }
    return new XMLSerializer().serializeToString(xmlDoc)
  }
}