import { nextTick } from 'vue'

/**
 * @typedef {Object} Section
 * @property {string} name
 * @property {string} dynamic
 * @property {string} directions
 */

/**
 * @typedef LineElement
 * @property {string} text
 * @property {string} chord
 * @property {newbar} boolean
 */

class Editing {
  /**
   * 
   * @param {Section[]} sections 
   * @param {Object.<string, LineElement[][]>} verses 
   */
  constructor(sections, verses) {
    this.sections = sections
    this.verses = verses
  }
  parseId(id) {
    const [sectionIndex, lineIndex, elementIndex, isChord] = id.split('-')
    return {
      section: parseInt(sectionIndex),
      line: parseInt(lineIndex),
      element: parseInt(elementIndex),
      isChord: !!isChord,
    }
  }
  formatId(id) {
    if (id.isChord) {
      return `${id.section}-${id.line}-${id.element}-chord`
    } else {
      return `${id.section}-${id.line}-${id.element}`
    }
  }
  findVerse(id) {
    const verseName = this.sections[id.section].name
    return this.verses[verseName]
  }
  findLine(id) {
    const verseName = this.sections[id.section].name
    return this.verses[verseName][id.line]
  }
  findElement(id) {
    const verseName = this.sections[id.section].name
    return this.verses[verseName][id.line][id.element]
  }
  addChord(idStr, selectionStart, forceNew = false) {
    const id = this.parseId(idStr)
    if (!forceNew && !id.isChord && selectionStart === 0) {
      return `${id.section}-${id.line}-${id.element}-chord`
    }
    const element = this.findElement(id)
    if (!forceNew && id.isChord && !element.chord && !element.newbar) {
      return `${id.section}-${id.line}-${id.element}-chord`
    }
    if (id.isChord) {
      selectionStart = 0
    }
    const line = this.findLine(id)
    const text = line[id.element].text
    const newElementOne = {
      text: text.substring(0, selectionStart),
      chord: line[id.element].chord,
      newbar: line[id.element].newbar
    }
    const newElementTwo = {
      text: text.substring(selectionStart)
    }
    line.splice(id.element, 1, newElementOne, newElementTwo)
    return `${id.section}-${id.line}-${id.element + 1}-chord`
  }
  addChordAtPreviousWord(idStr) {
    const id = this.parseId(idStr)
    if (id.element === 0) {
      // Move to previous line
      if (id.line > 0) {
        const verse = this.findVerse(id)
        document.getElementById(`${id.section}-${id.line - 1}-${verse[id.line - 1].length - 1}-chord`).focus()
      } else if (id.section > 0) {
        const verse = this.findVerse({ ...id, section: id.section - 1 })
        document.getElementById(`${id.section - 1}-${verse.length - 1}-${verse[verse.length - 1].length - 1}-chord`).focus()
      }
    } else {
      const line = this.findLine(id)
      const previousElement = line[id.element - 1]
      const rawPreviousSpaceIndex = previousElement.text.trimEnd().lastIndexOf(' ')
      if (rawPreviousSpaceIndex === -1 || /^ +$/.test(previousElement.text.substring(0, rawPreviousSpaceIndex))) {
        document.getElementById(`${id.section}-${id.line}-${id.element - 1}-chord`).focus()
      } else {
        const previousSpaceIndex = previousElement.text.substring(0, rawPreviousSpaceIndex).trimEnd().length
        const chordId = this.addChord(`${id.section}-${id.line}-${id.element - 1}`, previousSpaceIndex + 1)
        nextTick(() => {
          document.getElementById(chordId).focus()
        })
      }
    }
  }
  addChordAtNextWord(idStr) {
    const id = this.parseId(idStr)
    const line = this.findLine(id)
    const allText = line.map(element => element.text).reduce((a, b) => a + b)
    let textLengthBefore = 0
    if (id.element !== 0) {
      textLengthBefore = line.slice(0, id.element).map(element => element.text).reduce((a, b) => a + b).length
    }
    const nextWord = allText.match(new RegExp(`^.{${textLengthBefore}}[^ ]* +[^ ]`))
    // There is no next space
    if (nextWord === null) {
      if (id.element < line.length - 1) {
        // Move to existing chord that is after all text
        document.getElementById(`${id.section}-${id.line}-${id.element + 1}-chord`).focus()
      } else if (!line[id.element].text && !line[id.element].chord && !line[id.element].newbar) {
        // The caret is at the end of the line with no text underneath, go to next line
        const verse = this.findVerse(id)
        if (id.line + 1 <= verse.length - 1) {
          document.getElementById(`${id.section}-${id.line + 1}-0-chord`).focus()
        } else if (id.section + 1 <= this.sections.length - 1) {
          document.getElementById(`${id.section + 1}-0-0-chord`).focus()
        }
      } else {
        // Create new chord at the end of the line
        // Let the focusout event merge stuff if needed
        document.activeElement.blur()
        setTimeout(() => {
          const merged_line = this.findLine(id)
          const chordId = this.addChord(`${id.section}-${id.line}-${merged_line.length - 1}`, merged_line[merged_line.length - 1].text.length, true)
          nextTick(() => {
            document.getElementById(chordId).focus()
          })
        }, 0)
      }
    } else {
      const indexOfNextSpace = nextWord[0].length - 2
      // Let the focusout event merge stuff if needed
      document.activeElement.blur()
      setTimeout(() => {
        let endOfCurrentElement = -1
        let endOfPreviousElement = -1
        let chordId
        for (let i = 0; i < line.length; i++) {
          endOfCurrentElement = endOfCurrentElement + line[i].text.length
          if (endOfCurrentElement > textLengthBefore && endOfCurrentElement <= indexOfNextSpace) {
            // We are after the current chord, but the next chord is before the next space
            // So switch to that chord instead of to the space
            nextTick(() => {
              document.getElementById(`${id.section}-${id.line}-${i + 1}-chord`).focus()
            })
            return
          }
          if (endOfCurrentElement > indexOfNextSpace) {
            // Next space is inside this element, create chord
            chordId = this.addChord(`${id.section}-${id.line}-${i}`, indexOfNextSpace - endOfPreviousElement)
            nextTick(() => {
              document.getElementById(chordId).focus()
            })
            return
          }
          endOfPreviousElement = endOfCurrentElement
        }
      }, 0)
    }
  }
  addLine(idStr, caretPosition) {
    const id = this.parseId(idStr)
    const verse = this.findVerse(id)
    let newLineFirstElement = null
    if (caretPosition !== 0 || verse[id.line][id.element].text.length === 0) {
      this.addChord(idStr, caretPosition)
      newLineFirstElement = id.element + 1
    } else {
      newLineFirstElement = id.element
    }
    const elementsToMove = verse[id.line].splice(newLineFirstElement, verse[id.line].length)
    if (elementsToMove.length === 0) {
      elementsToMove.push({ text: "" })
    }
    verse.splice(id.line + 1, 0, elementsToMove)
    return `${id.section}-${id.line + 1}-0`
  }
  toggleBarLine(idStr, selectionStart) {
    let idToUse = null
    const id = this.parseId(idStr)
    if (id.isChord) {
      idToUse = id
    } else if (selectionStart !== 0) {
      const newElementId = this.addChord(idStr, selectionStart)
      idToUse = this.parseId(newElementId)
    } else {
      idToUse = this.parseId(idStr)
    }
    const element = this.findElement(idToUse)
    if (element.newbar) {
      element.newbar = false
    } else {
      element.newbar = true
    }
    if (id.isChord) {
      return { id: `${idToUse.section}-${idToUse.line}-${idToUse.element}-chord`, usePreviousIndex: true }
    }
    return { id: `${idToUse.section}-${idToUse.line}-${idToUse.element}-chord`, usePreviousIndex: false }
  }
  removeBarLine(idStr) {
    const id = this.parseId(idStr)
    const element = this.findElement(id)
    if (element.newbar) {
      element.newbar = false
      return true
    } else {
      return false
    }

  }
  deleteElement(idStr) {
    const id = this.parseId(idStr)
    const line = this.findLine(id)
    if (line === undefined) {
      // Can happen due to race conditions between ChordLyrics.checkDelete and Backspace
      return
    } else if (line.length === 1) {
      // Never delete the last element, otherwise you can't edit anymore
      return
    }
    line.splice(id.element, 1)
  }
  mergeElement(idStr) {
    const id = this.parseId(idStr)
    const line = this.findLine(id)
    if (id.element > line.length - 1) {
      // This can happen because the focusout event fires for an input
      // while the related data is already gone
      return
    }
    const newText = line[id.element - 1].text + line[id.element].text
    line.splice(id.element, 1)
    line[id.element - 1].text = newText
    return `${id.section}-${id.line}-${id.element - 1}`
  }
  mergeLine(idStr, withNext = false) {
    let id = this.parseId(idStr)
    if (withNext) {
      id = { ...id, line: id.line + 1 }
    }
    const line = this.findLine(id)
    const verse = this.findVerse(id)
    const previousLineElements = verse[id.line - 1].length - 1
    // Add space to previous line if needed
    const previousLineText = verse[id.line - 1][previousLineElements].text
    if (!previousLineText.endsWith(' ')) {
      verse[id.line - 1][previousLineElements].text = `${previousLineText} `
    }
    verse[id.line - 1].push(...line)
    verse.splice(id.line, 1)
    return `${id.section}-${id.line - 1}-${previousLineElements + 1}`
  }
  addSection(afterIdStr) {
    let newSectionId
    if (afterIdStr === null) {
      newSectionId = 0
    } else {
      const id = this.parseId(afterIdStr)
      newSectionId = id.section + 1
    }
    const newSection = { directions: "", name: "" }
    this.sections.splice(newSectionId, 0, newSection)
    return `section-${newSectionId}-name`
  }
  addVerse(verseName, sectionIndex) {
    if (this.sections[sectionIndex].name === "") {
      this.verses[verseName] = [[{ text: '' }]]
    } else {
      this.verses[verseName] = JSON.parse(JSON.stringify(this.verses[this.sections[sectionIndex].name]))
    }
    this.sections[sectionIndex].name = verseName
    return `${sectionIndex}-0-0`
  }
  renameVerse(newVerseName, oldVerseName) {
    this.verses[newVerseName] = this.verses[oldVerseName]
    for (let section of this.sections) {
      if (section.name === oldVerseName) {
        section.name = newVerseName
      }
    }
    delete this.verses[oldVerseName]
  }
  changeVerse(newVerseName, sectionIndex) {
    this.sections[sectionIndex].name = newVerseName
  }
  pasteMultipleLines(text, idStr, selectionStart) {
    let elementId = this.parseId(idStr)
    let element = this.findElement(elementId)
    const lines = text.split(/\r?\n/)
    for (let i = 0; i < lines.length; i++) {
      let line = lines[i]
      element.text = element.text.substring(0, selectionStart) + line + element.text.substring(selectionStart)
      if (i !== lines.length - 1) {
        elementId = this.parseId(this.addLine(`${elementId.section}-${elementId.line}-${elementId.element}`, selectionStart + line.length))
        element = this.findElement(elementId)
      }
      selectionStart = 0
    }
    return { id: `${elementId.section}-${elementId.line}-${elementId.element}`, focusStart: lines[lines.length - 1].length }
  }
  moveSection(sectionIndex, afterSectionIndex) {
    const section = this.sections[sectionIndex]
    this.sections.splice(afterSectionIndex + 1, 0, section)
    if (sectionIndex < afterSectionIndex) {
      this.sections.splice(sectionIndex, 1)
    } else {
      this.sections.splice(sectionIndex + 1, 1)
    }
  }
  removeSection(sectionIndex) {
    this.sections.splice(sectionIndex, 1)
  }
  moveSpaces(idStr) {
    const id = this.parseId(idStr)
    const currentElement = this.findElement(id)
    const previousElement = this.findElement({ ...id, element: id.element - 1 })
    if (currentElement.text.startsWith(" ") && !previousElement.text.endsWith(" ")) {
      previousElement.text = previousElement.text + " "
    }
  }
}

export default Editing