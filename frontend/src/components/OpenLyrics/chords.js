
const orderSharp = ['A', 'A#', 'B', 'C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#']
const orderFlat = ['A', 'Bb', 'B', 'C', 'Db', 'D', 'Eb', 'E', 'F', 'Gb', 'G', 'Ab']

const structures = {
  major: /^$/,
  minor: /^m$/,
  sus4: /^(sus|sus4)$/,
  sus2: /^2$/,
  minor7: /^m7$/
}

export default {
  chordRegex: /^(?<root>[A-G][#b]?)(?<structure>7?(m|min|dim|sus|maj|add|aug|dom)?(7|2|5|4|7b5|6|9|11|13){0,2})(\/(?<bass>[A-G][#b]?))?$/,
  stringifyChord(chord) {
    let chordStr = chord.root
    if (chord.structure) {
      chordStr = chordStr + chord.structure
    }
    if (chord.bass) {
      chordStr = chordStr + '/' + chord.bass
    }
    return chordStr
  },
  parseChord(chordStr) {
    const result = chordStr.match(this.chordRegex)
    return {
      root: result.groups.root,
      structure: result.groups.structure,
      bass: result.groups.bass
    }
  },
  transposeNote(note, transposeAmount, useFlats) {
    let index = orderSharp.indexOf(note)
    if (index === -1) {
      index = orderFlat.indexOf(note)
    }
    let newIndex = (index + transposeAmount) % 12
    if (newIndex < 0) {
      newIndex = newIndex + 12
    }
    if (useFlats) {
      return orderFlat[newIndex]
    }
    return orderSharp[newIndex]
  },
  transpose(chordStr, transposeAmount, useFlats) {
    const chord = this.parseChord(chordStr)
    chord.root = this.transposeNote(chord.root, transposeAmount, useFlats)
    if (chord.bass) {
      chord.bass = this.transposeNote(chord.bass, transposeAmount, useFlats)
    }
    return this.stringifyChord(chord)
  },
  getNotes(chordStr, useFlats) {
    const chord = this.parseChord(chordStr)
    if (structures.major.test(chord.structure)) {
      return [
        chord.root,
        this.transposeNote(chord.root, 4, useFlats),
        this.transposeNote(chord.root, 7, useFlats)]
    } else if (structures.minor.test(chord.structure)) {
      return [
        chord.root,
        this.transposeNote(chord.root, 3, useFlats),
        this.transposeNote(chord.root, 7, useFlats)]
    } else if (structures.minor7.test(chord.structure)) {
      return [
        chord.root,
        this.transposeNote(chord.root, 3, useFlats),
        this.transposeNote(chord.root, 7, useFlats),
        this.transposeNote(chord.root, 10, useFlats)]
    } else if (structures.sus4.test(chord.structure)) {
      return [
        chord.root,
        this.transposeNote(chord.root, 5, useFlats),
        this.transposeNote(chord.root, 7, useFlats)]
    } else if (structures.sus2.test(chord.structure)) {
      return [
        chord.root,
        this.transposeNote(chord.root, 2, useFlats),
        this.transposeNote(chord.root, 7, useFlats)]
    } else {
      console.log('Unknown chord structure', chordStr)
      return [
        chord.root,
        this.transposeNote(chord.root, 7, useFlats)]
    }
  }
}