import apiStore from '@/shared-vue/apiStore'
import api from '@/shared-vue/api'

export default {
  namespaced: true,
  state: {
    selectedBand: null,
    playRecording: null,
  },
  actions: {
    playRecording(context, recording) {
      context.commit('setRecording', recording)
    },
    closeRecording(context) {
      context.commit('setRecording', null)
    },
    setBand(context, band) {
      this.commit('tehila/events/clearAll')
      this.commit('tehila/songs/clearAll')
      this.commit('tehila/bandSongs/clearAll')
      if (band === null) {
        api.extraHeaders = {}
        context.commit('selectBand', null)
      } else {
        api.extraHeaders = { 'x-tehila-band': band.id }
        context.commit('selectBand', band)
      }
    }
  },
  mutations: {
    initialize(state) {
      this.commit('tehila/songs/initialize')
      this.commit('tehila/collections/initialize')
      this.commit('tehila/events/initialize')
      this.commit('tehila/bandSongs/initialize')
      this.commit('tehila/tags/initialize')
      this.commit('tehila/lyrics/initialize')
      this.commit('tehila/verses/initialize')
      const tabBand = window.sessionStorage.getItem("band")
      if (tabBand !== null && tabBand !== 'null') {
        const band = JSON.parse(tabBand)
        api.extraHeaders = { 'x-tehila-band': band.id }
        state.selectedBand = band
      }
    },
    selectBand(state, band) {
      window.sessionStorage.setItem("band", JSON.stringify(band))
      state.selectedBand = band
    },
    setRecording(state, recording) {
      state.playRecording = recording
    }
  },
  modules: {
    songs: apiStore('api/songs/'),
    collections: apiStore('api/collections/', 30000),
    events: apiStore('api/events/'),
    bandSongs: apiStore('api/bandsongs/'),
    tags: apiStore('api/tags/', 30000),
    lyrics: apiStore('api/lyrics/'),
    verses: apiStore('api/verses/'),
    invites: apiStore('api/invites/')
  }
}