import { createStore } from 'vuex'
import Auth from '@/shared-vue/authStore'
import notificationStore from '@/shared-vue/notifications/store'
import Tehila from './tehila'


export default createStore({
  mutations: {
    initializeStore() {
      this.commit('tehila/initialize')
    }
  },
  modules: {
    auth: Auth,
    tehila: Tehila,
    notify: notificationStore
  }
})

