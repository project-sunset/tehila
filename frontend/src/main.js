import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import api from './shared-vue/api'
import notify from './shared-vue/notifications/notify'

import "@/assets/main.scss"

const app = createApp(App)
app.use(router)
app.use(store)

const notifier = notify(store)
api.notify = notifier

app.config.globalProperties.$api = api
app.config.globalProperties.$notify = notifier
app.config.globalProperties.$filters = {
  currency(value, na) {
    if (na && !value) return 'NA'
    if (!value) value = 0
    value = Number(value)
    return value.toLocaleString('nl-NL', { style: 'currency', currency: 'EUR' })
  },
  dutchDateTime(value) {
    return new Date(value).toLocaleDateString('nl-NL', { year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric' })
  },
  dutchDate(value) {
    return new Date(value).toLocaleDateString('nl-NL', { year: 'numeric', month: 'long', day: 'numeric' })
  }
}

app.mount("#app")