const Base = () => import('@/components/Base')
const Home = () => import('@/views/Home')
const Songs = () => import('@/views/Songs')
const Calendar = () => import('@/views/Calendar')
const Event = () => import('@/views/Event')
const InternalRepertoire = () => import('@/views/InternalRepertoire')
const Recorder = () => import('@/views/Recorder')
const TagManager = () => import('@/views/TagManager')
const IcsExplanation = () => import('@/views/IcsExplanation')
const BandOpenLyricsEditor = () => import('@/views/BandOpenLyricsEditor')
const EventSheetMusic = () => import('@/views/EventSheetMusic')
const Invites = () => import('@/views/Invites')
const BandSettings = () => import('@/views/BandSettings')
const BandStats = () => import('@/views/BandStats')
const PublicOpenLyrics = () => import('@/views/PublicOpenLyrics')
const PublicOpenLyricsEditor = () => import('@/views/PublicOpenLyricsEditor')
const PdfMusicViewer = () => import('@/views/PdfMusicViewer')
import AccountRoutes from '@/shared-vue/router/AccountRoutes'


export default [
  {
    path: '/',
    component: Base,
    children: [
      { path: '', component: Home, name: 'tehila:home', },
      { path: 'collections', component: Songs, name: 'tehila:collections' },
      { path: 'calendar', component: Calendar, name: 'tehila:calendar' },
      { path: 'recorder', component: Recorder, name: 'tehila:recorder' },
      { path: 'repertoire', component: InternalRepertoire, name: 'tehila:internal-repertoire' },
      { path: 'event/:id', component: Event, name: 'tehila:event', props: true },
      { path: 'event/:id/sheet-music', component: EventSheetMusic, name: 'tehila:event-sheet-music', props: true },
      { path: 'tags', component: TagManager, name: 'tehila:tag-manager' },
      { path: 'ics', component: IcsExplanation, name: 'tehila:ics' },
      { path: 'invites', component: Invites, name: 'tehila:invites' },
      { path: 'openlyrics/:songId/:existingOpenLyricsId', component: BandOpenLyricsEditor, name: 'tehila:open-lyrics-editor', props: true },
      { path: 'openlyrics/:songId', component: BandOpenLyricsEditor, name: 'tehila:open-lyrics-editor-new', props: true },
      { path: 'pdf/:pdfMusicId', component: PdfMusicViewer, name: 'tehila:pdf-viewer', props: route => ({ editable: true, pdfMusicId: route.params.pdfMusicId }) },
      { path: 'settings', component: BandSettings, name: 'tehila:band-settings' },
      { path: 'stats', component: BandStats, name: 'tehila:band-stats' },
      ...AccountRoutes({ backLocation: { name: 'tehila:home' } })
    ]
  },
  {
    path: '/repertoire/:bandname', component: Home, beforeEnter(to, from) {
      window.location.href = `https://tehila.projectsunset.nl/bands/${encodeURIComponent(to.params.bandname)}/`
    },
  },
  {
    path: '/public/openlyrics', component: PublicOpenLyrics, name: 'tehila:public:list'
  },
  {
    path: '/public/openlyrics/editor/:id/', component: PublicOpenLyricsEditor, name: 'tehila:public:editor', props: true
  },
  // Redirect from old /tehila URLs to /
  {
    path: '/tehila', redirect: '/'
  },
  {
    path: '/tehila/:something', redirect: to => {
      return to.path.replace('/tehila', '')
    }
  },
  {
    path: '/tehila/:something/:somethingelse', redirect: to => {
      return to.path.replace('/tehila', '')
    },
  },
]
