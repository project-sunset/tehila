import { createRouter, createWebHashHistory } from 'vue-router'
import TehilaRoutes from '@/router/TehilaRoutes.js'


export default createRouter({
  history: createWebHashHistory(),
  routes: [
    ...TehilaRoutes,
  ],
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
})
