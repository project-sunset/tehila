
var Module = (() => {
  var _scriptDir = import.meta.url;
  
  return (
function(config) {
  var Module = config || {};


var a;a||(a=typeof Module !== 'undefined' ? Module : {});var q,r;a.ready=new Promise(function(b,k){q=b;r=k});var u=Object.assign({},a),w="";"undefined"!=typeof document&&document.currentScript&&(w=document.currentScript.src);_scriptDir&&(w=_scriptDir);0!==w.indexOf("blob:")?w=w.substr(0,w.replace(/[?#].*/,"").lastIndexOf("/")+1):w="";var x=a.printErr||console.warn.bind(console);Object.assign(a,u);u=null;var y;a.wasmBinary&&(y=a.wasmBinary);var noExitRuntime=a.noExitRuntime||!0;
"object"!=typeof WebAssembly&&z("no native wasm support detected");var B,C=!1,D="undefined"!=typeof TextDecoder?new TextDecoder("utf8"):void 0,E,F,G=[],H=[],I=[];function J(){var b=a.preRun.shift();G.unshift(b)}var K=0,L=null,M=null;function z(b){if(a.onAbort)a.onAbort(b);b="Aborted("+b+")";x(b);C=!0;b=new WebAssembly.RuntimeError(b+". Build with -sASSERTIONS for more info.");r(b);throw b;}function N(){return O.startsWith("data:application/octet-stream;base64,")}var O;
if(a.locateFile){if(O="match_score.wasm",!N()){var P=O;O=a.locateFile?a.locateFile(P,w):w+P}}else O=(new URL("match_score.wasm",import.meta.url)).href;function Q(){var b=O;try{if(b==O&&y)return new Uint8Array(y);throw"both async and sync fetching of the wasm failed";}catch(k){z(k)}}
function R(){return y||"function"!=typeof fetch?Promise.resolve().then(function(){return Q()}):fetch(O,{credentials:"same-origin"}).then(function(b){if(!b.ok)throw"failed to load wasm binary file at '"+O+"'";return b.arrayBuffer()}).catch(function(){return Q()})}function S(b){for(;0<b.length;)b.shift()(a)}
function T(b,k,n,p){var d={string:c=>{var m=0;if(null!==c&&void 0!==c&&0!==c){var h=(c.length<<2)+1;m=V(h);var f=m,e=F;if(0<h){h=f+h-1;for(var t=0;t<c.length;++t){var g=c.charCodeAt(t);if(55296<=g&&57343>=g){var aa=c.charCodeAt(++t);g=65536+((g&1023)<<10)|aa&1023}if(127>=g){if(f>=h)break;e[f++]=g}else{if(2047>=g){if(f+1>=h)break;e[f++]=192|g>>6}else{if(65535>=g){if(f+2>=h)break;e[f++]=224|g>>12}else{if(f+3>=h)break;e[f++]=240|g>>18;e[f++]=128|g>>12&63}e[f++]=128|g>>6&63}e[f++]=128|g&63}}e[f]=0}}return m},
array:c=>{var m=V(c.length);E.set(c,m);return m}};b=a["_"+b];var l=[],A=0;if(p)for(var v=0;v<p.length;v++){var U=d[n[v]];U?(0===A&&(A=W()),l[v]=U(p[v])):l[v]=p[v]}n=b.apply(null,l);return n=function(c){0!==A&&X(A);if("string"===k)if(c){for(var m=F,h=c+NaN,f=c;m[f]&&!(f>=h);)++f;if(16<f-c&&m.buffer&&D)c=D.decode(m.subarray(c,f));else{for(h="";c<f;){var e=m[c++];if(e&128){var t=m[c++]&63;if(192==(e&224))h+=String.fromCharCode((e&31)<<6|t);else{var g=m[c++]&63;e=224==(e&240)?(e&15)<<12|t<<6|g:(e&7)<<
18|t<<12|g<<6|m[c++]&63;65536>e?h+=String.fromCharCode(e):(e-=65536,h+=String.fromCharCode(55296|e>>10,56320|e&1023))}}else h+=String.fromCharCode(e)}c=h}}else c="";else c="boolean"===k?!!c:c;return c}(n)}var ba={};
(function(){function b(d){a.asm=d.exports;B=a.asm.a;d=B.buffer;a.HEAP8=E=new Int8Array(d);a.HEAP16=new Int16Array(d);a.HEAP32=new Int32Array(d);a.HEAPU8=F=new Uint8Array(d);a.HEAPU16=new Uint16Array(d);a.HEAPU32=new Uint32Array(d);a.HEAPF32=new Float32Array(d);a.HEAPF64=new Float64Array(d);H.unshift(a.asm.b);K--;a.monitorRunDependencies&&a.monitorRunDependencies(K);0==K&&(null!==L&&(clearInterval(L),L=null),M&&(d=M,M=null,d()))}function k(d){b(d.instance)}function n(d){return R().then(function(l){return WebAssembly.instantiate(l,
p)}).then(function(l){return l}).then(d,function(l){x("failed to asynchronously prepare wasm: "+l);z(l)})}var p={a:ba};K++;a.monitorRunDependencies&&a.monitorRunDependencies(K);if(a.instantiateWasm)try{return a.instantiateWasm(p,b)}catch(d){x("Module.instantiateWasm callback failed with error: "+d),r(d)}(function(){return y||"function"!=typeof WebAssembly.instantiateStreaming||N()||"function"!=typeof fetch?n(k):fetch(O,{credentials:"same-origin"}).then(function(d){return WebAssembly.instantiateStreaming(d,
p).then(k,function(l){x("wasm streaming compile failed: "+l);x("falling back to ArrayBuffer instantiation");return n(k)})})})().catch(r);return{}})();a.___wasm_call_ctors=function(){return(a.___wasm_call_ctors=a.asm.b).apply(null,arguments)};a._match_score=function(){return(a._match_score=a.asm.c).apply(null,arguments)};
var W=a.stackSave=function(){return(W=a.stackSave=a.asm.e).apply(null,arguments)},X=a.stackRestore=function(){return(X=a.stackRestore=a.asm.f).apply(null,arguments)},V=a.stackAlloc=function(){return(V=a.stackAlloc=a.asm.g).apply(null,arguments)};a.ccall=T;a.cwrap=function(b,k,n,p){n=n||[];var d=n.every(l=>"number"===l||"boolean"===l);return"string"!==k&&d&&!p?a["_"+b]:function(){return T(b,k,n,arguments,p)}};var Y;M=function ca(){Y||Z();Y||(M=ca)};
function Z(){function b(){if(!Y&&(Y=!0,a.calledRun=!0,!C)){S(H);q(a);if(a.onRuntimeInitialized)a.onRuntimeInitialized();if(a.postRun)for("function"==typeof a.postRun&&(a.postRun=[a.postRun]);a.postRun.length;){var k=a.postRun.shift();I.unshift(k)}S(I)}}if(!(0<K)){if(a.preRun)for("function"==typeof a.preRun&&(a.preRun=[a.preRun]);a.preRun.length;)J();S(G);0<K||(a.setStatus?(a.setStatus("Running..."),setTimeout(function(){setTimeout(function(){a.setStatus("")},1);b()},1)):b())}}
if(a.preInit)for("function"==typeof a.preInit&&(a.preInit=[a.preInit]);0<a.preInit.length;)a.preInit.pop()();Z();


  return Module.ready
}
);
})();
export default Module;