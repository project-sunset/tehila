#     Tehila, church band management
#     Copyright (C) 2023  Bob Luursema

#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Affero General Public License as published
#     by the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.

#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Affero General Public License for more details.

#     You should have received a copy of the GNU Affero General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

ARG PIPELINE_VERSION
FROM registry.gitlab.com/project-sunset/shared/django:${PIPELINE_VERSION}

RUN apt update && apt install -y poppler-utils imagemagick && apt clean