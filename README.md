# Tehila

Hebrew name meaning 'glory' or 'praise'

This is a webapp to manage the repertoire, recordings and photo's of my band.

Find the deployed application here: https://tehila.projectsunset.nl/

## Testing

Test users:
- Superuser: David:harp
- user: Jubal:lyre

### Create E2E tests

To start creating E2E tests in VS Code
- Press F1, execute task: "Refresh DB"
- Press F5 to run the "compound" lauch configuration
- Press F1, execute task: "npm: codegen - tests"

Now just click around to create the necessary code.

If new database fixtures are needed, run the database refresh, then add the necessary stuff on the website and then run the `store_fixtures.sh` script.