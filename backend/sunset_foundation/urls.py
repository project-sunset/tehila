from django.shortcuts import redirect
from django.urls import path

from .views import (index, manifest_json, robots_txt, service_worker,
                    webmanifest, workbox, remove_sw)

urlpatterns = [
    path('app/', index, name="home"),
    path('favicon.ico', lambda r: redirect('/static/favicon.ico')),
    path('app/index.html', index, name="home2"),
    path('service-worker.js', remove_sw),
    path('robots.txt', robots_txt),
    path('app/service-worker.js', service_worker, name="service_worker"),
    path('app/manifest.json', manifest_json),
    path('app/manifest.webmanifest', webmanifest),
    path('app/workbox-<str:hashed_url>.js', workbox),
]
