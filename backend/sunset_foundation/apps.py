from django.apps import AppConfig


class SunsetFoundationConfig(AppConfig):
    name = 'sunset_foundation'
