from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import ensure_csrf_cookie


@ensure_csrf_cookie
def index(request):
    if hasattr(settings, 'GOOGLE_CLIENT_ID'):
        context = {'name': 'google-signin-client_id', 'google_client_id': settings.GOOGLE_CLIENT_ID}
    else:
        context = {}
    return render(request, 'index.html', context)


def robots_txt(request):
    return render(request, 'robots.txt', content_type='text/plain')


def service_worker(request):
    return render(request, 'service-worker.js', content_type="application/javascript")


def manifest_json(request):
    return render(request, 'manifest.json', content_type="application/json")


def webmanifest(request):
    return render(request, 'manifest.webmanifest', content_type="application/manifest+json")


def workbox(request, hashed_url):
    return render(request, f'workbox-{hashed_url}.js', content_type="application/javascript")


def remove_sw(_):
    return HttpResponse(content='''
    self.addEventListener('install', function(e) {
  self.skipWaiting();
});

self.addEventListener('activate', function(e) {
  self.registration.unregister()
    .then(function() {
      return self.clients.matchAll();
    })
    .then(function(clients) {
      clients.forEach(client => client.navigate(client.url))
    });
});
    ''', content_type='text/javascript')
