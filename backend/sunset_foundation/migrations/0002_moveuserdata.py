from django.db import migrations


def move_google_user(apps, schema_editor):
    Sunsetuser = apps.get_model('sunset_foundation', 'SunsetUser')
    Googleuser = apps.get_model('shared_django', 'GoogleUser')
    for user in Sunsetuser.objects.all():
        newuser = Googleuser(user=user.user, google_id=user.google_id)
        newuser.save()


class Migration(migrations.Migration):

    dependencies = [
        ('sunset_foundation', '0001_initial'),
        ('shared_django', '0001_initial')
    ]

    operations = [
        migrations.RunPython(move_google_user),
    ]
