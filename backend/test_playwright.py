from django.test import tag
from shared_django.testutils import PlaywrightTests


@tag('playwright')
class FrontendTests(PlaywrightTests):
    fixtures = [
        'users_test_data.json',
        'songs_test_data.json',
        'events_test_data.json',
        'bands_test_data.json',
        'beamer_password.json'
    ]
    config = {
        'username': 'David',
        'password': 'harp',
        'username2': 'Jubal',
        'password2': 'lyre',
    }

    @tag('songs-ui')
    def test_songs(self):
        self.run_playwright('songs')

    @tag('bands-ui')
    def test_bands(self):
        self.run_playwright('bands')

    @tag('events-ui')
    def test_events(self):
        self.run_playwright('events')

    @tag('recordings-ui')
    def test_recordings(self):
        self.run_playwright('recordings')
    
    @tag('sites-ui')
    def test_sites(self):
        self.run_playwright('sites')
    
    @tag('openlyrics-ui')
    def test_openlyrics(self):
        self.run_playwright('openlyrics')
    
    @tag('pdf-ui')
    def test_pdf(self):
        self.run_playwright('pdf')