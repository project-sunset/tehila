from django import forms

from tehila.songs.models import Collection

class CollectionSearchForm(forms.Form):
    collection = forms.ChoiceField(choices=lambda: Collection.objects.all().values_list('id', 'title'))
    number = forms.CharField(widget=forms.TextInput(attrs={"class": "input"}))

class TitleSearchForm(forms.Form):
    title = forms.CharField(widget=forms.TextInput(attrs={"class": "input"}))