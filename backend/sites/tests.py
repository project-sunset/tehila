from django.test import Client, TestCase, tag


@tag('sites')
class SiteTests(TestCase):
    fixtures = [
        'users_test_data.json',
        'bands_test_data.json',
        'songs_test_data.json',
        'events_test_data.json',
        'beamer_password.json'
    ]

    def test_homepage(self):
        c = Client()
        response = c.get('/')
        self.assertEqual(response.status_code, 200)

    def test_church_page(self):
        c = Client()
        response = c.get('/church/The ancients/')
        self.assertEqual(response.status_code, 200)

    def test_band_page(self):
        c = Client()
        response = c.get('/bands/Original Psalm Project/')
        self.assertEqual(response.status_code, 200)

    def test_disclaimer(self):
        c = Client()
        response = c.get('/disclaimer/')
        self.assertEqual(response.status_code, 200)

    def test_docs(self):
        c = Client()
        response = c.get('/docs/')
        self.assertEqual(response.status_code, 200)

    def test_contact_forms(self):
        c = Client()
        response = c.post('/contact/', {'message': 'flubber'}, content_type="application/json")
        self.assertEqual(response.status_code, 204)

    def test_unread_messages(self):
        c = Client()
        c.login(username='David', password='harp')
        response = c.get('/contact/unread/')
        self.assertEqual(response.status_code, 200)
    
    def test_beamer_pwd(self):
        c = Client()
        response = c.get('/beamer/password/')
        self.assertEqual(response.status_code, 302)
        c.login(username='David', password='harp')
        response = c.get('/beamer/password/')
        self.assertEqual(response.status_code, 200)
        response = c.post('/beamer/password/', {'password1': '12345678', 'password2': '12345678'})
        self.assertEqual(response.status_code, 200)        
    
    def test_beamer_login(self):
        c = Client()
        response = c.get('/beamer/login/')
        self.assertEqual(response.status_code, 200)
        response = c.post('/beamer/login/', {'password': 'abcdefgh'})
        self.assertEqual(response.status_code, 302)

    def test_beamer_not_logged_in(self):
        c = Client()
        response = c.get('/beamer/lyrics/')
        self.assertEqual(response.status_code, 302)
    
    def test_beamer_lyrics(self):
        c = Client()
        session = c.session
        session['beamer_pwd_ok'] = True
        session.save()
        response = c.get('/beamer/lyrics/')
        self.assertEqual(response.status_code, 200)