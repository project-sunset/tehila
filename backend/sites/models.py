from django.db import models


class ContactForm(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=100, null=True, blank=True)
    message = models.TextField(max_length=500)
    created_time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.name} - {self.created_time}'

class BeamerPassword(models.Model):
    password = models.CharField(max_length=200)