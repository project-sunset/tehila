import json
import xml.etree.ElementTree as ET
from datetime import date
from urllib.parse import quote

from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.hashers import check_password, make_password
from django.http import HttpRequest, HttpResponse, JsonResponse
from django.shortcuts import redirect, render
from tehila.songs.lyrics_processor import get_content, long_verse_name
from sites.forms import CollectionSearchForm, TitleSearchForm
from sites.models import BeamerPassword, ContactForm
from tehila.bands.models import Band, Church
from tehila.bands.views import get_repertoire
from tehila.songs.models import Collection, Lyrics, Song, SongInCollection
from tehila.songs.song_search import find_matching_songs

MAX_CONTACT_FORM = 100


@user_passes_test(lambda u: u.is_superuser)
def set_beamer_password(request: HttpRequest):
    template = 'sites/beamer-lyrics/password.html'
    if request.method == 'POST':
        if request.POST['password1'] != request.POST['password2'] or len(request.POST['password1']) < 8:
            return render(request, template, {'wrong': True})
        pwd = make_password(request.POST['password1'])
        BeamerPassword.objects.all().delete()
        BeamerPassword.objects.create(password=pwd)
        return render(request, template, {'done': True})
    else:
        return render(request, template)


def beamer_login_page(request: HttpRequest):
    request.session['beamer_pwd_ok'] = False
    if request.method == 'POST':
        if check_password(request.POST['password'], BeamerPassword.objects.all()[0].password):
            request.session['beamer_pwd_ok'] = True
            return redirect('beamer_lyrics')
    return render(request, 'sites/beamer-lyrics/login.html')

def beamer_lyrics(request: HttpRequest):
    if 'beamer_pwd_ok' not in request.session or request.session['beamer_pwd_ok'] != True:
        return redirect('beamer_login_page')
    context = {
        'collections': Collection.objects.all().values_list('id', 'title'),
        'collection_search_form': CollectionSearchForm(),
        'title_search_form': TitleSearchForm()
    }
    if 'collection-search' in request.GET:
        form = CollectionSearchForm(request.GET)
        context['collection_search_form'] = form
        if form.is_valid():
            match = SongInCollection.objects.filter(number__iexact=form.cleaned_data['number'], collection=form.cleaned_data['collection'])
            if match.exists():
                context['song'] = match[0].song
            else:
                context['no_match'] = True
    if 'title-search' in request.GET:
        form = TitleSearchForm(request.GET)
        context['title_search_form'] = form
        if form.is_valid():
            matches = find_matching_songs(form.cleaned_data['title'])[0]
            if len(matches) == 1:
                context['song'] = matches[0]['song']
            elif len(matches) == 0:
                context['no_match'] = True
            else:
                context['total_matches'] = len(matches)
                context['matches'] = [s['song'] for s in matches[:10]]
    if 'id' in request.GET:
        context['song'] = Song.objects.get(id=request.GET['id'])
    if 'song' in context:
        lyrics = Lyrics.objects.filter(song=context['song'])
        if lyrics.exists():
            content = get_content(ET.fromstring(lyrics[0].xml))
            context['lyrics'] = [(long_verse_name(verse_name), "\n".join(content['verses'].get(verse_name, []))) for verse_name in content['verse_order']]
        else:
            context['no_lyrics'] = True
    return render(request, 'sites/beamer-lyrics/search.html', context)


def home_page(request: HttpRequest):
    churches = Church.objects.all()
    contact_form_closed = ContactForm.objects.count() >= MAX_CONTACT_FORM
    return render(request, 'sites/home.html', {'churches': churches, 'contact_form_closed': contact_form_closed})


def church_page(request: HttpRequest, church_name: str):
    church = Church.objects.get(name=church_name)
    bands = Band.objects.filter(church=church)
    return render(request, 'sites/church.html', {'church': church, 'bands': bands})


def band_page(request: HttpRequest, band_name: str):
    band = Band.objects.get(name=band_name)
    repertoire = get_repertoire(band.id)
    collections = set()
    tags = set()
    song_data = []
    for song in repertoire:
        song['collection_text'] = ', '.join(map(lambda col: col['collection'] if col['number'] == 0 or col['number'] is None else f"{col['collection']} {col['number']}", song['collections']))
        song['last_played_text'] = date.fromisoformat(song['last_played'][:10]).strftime('%d-%m-%Y')
        song['lyrics_search'] = f'https://www.google.com/search?q=liedtekst%20{quote(song["title"])}'
        song['spotify_search'] = f'https://open.spotify.com/search/{quote(song["title"])}'
        song['youtube_search'] = f'https://www.youtube.com/results?search_query={quote(song["title"])}'
        collections.update(map(lambda col: col['collection'], song['collections']))
        tags.update(map(lambda tag: tag['name'], song['tags']))
        song_data.append({
            'id': song['id'],
            'title': song['title'],
            'collections': song['collections'],
            'tags': list(map(lambda tag: tag['name'], song['tags']))
        })
    contact_form_closed = ContactForm.objects.count() >= MAX_CONTACT_FORM
    return render(request, 'sites/band.html', {
        'band': band,
        'repertoire': repertoire,
        'collections': collections,
        'tags': tags,
        'song_data': {'data': song_data},
        'contact_form_closed': contact_form_closed
    })


def docs(request: HttpRequest):
    return render(request, 'sites/docs.html')

def sheet_music_docs(request: HttpRequest):
    return render(request, 'sites/sheet-music-docs.html')


def contact_form(request: HttpRequest):
    if request.method != 'POST':
        return HttpResponse(status=405)
    if not request.user.is_authenticated and ContactForm.objects.count() >= MAX_CONTACT_FORM:
        return HttpResponse(status=500)
    data = json.loads(request.body.decode('utf8'))
    name = data['name'] if 'name' in data else 'Anonymous'
    email = data['email'] if 'email' in data else None
    form = ContactForm(
        name=name,
        email=email,
        message=data['message'])
    form.save()
    return HttpResponse(status=204)


@user_passes_test(lambda u: u.is_superuser)
def unread_contact_forms(_: HttpRequest):
    contact_forms = ContactForm.objects.count()
    return JsonResponse({'unread_messsages': contact_forms})


def disclaimer(request: HttpRequest):
    return render(request, 'sites/privacy_and_disclaimer.html')
