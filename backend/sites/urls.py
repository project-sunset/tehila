from django.urls import path
from sites.views import (band_page, beamer_login_page, beamer_lyrics,
                         church_page, contact_form, disclaimer, docs,
                         home_page, set_beamer_password, sheet_music_docs,
                         unread_contact_forms)

urlpatterns = [
    path('', home_page, name='home_page'),
    path('docs/', docs, name='docs'),
    path('sheet-music-docs/', sheet_music_docs, name='sheet_music_docs'),
    path('church/<str:church_name>/', church_page, name='church_page'),
    path('bands/<str:band_name>/', band_page, name='band_page'),
    path('contact/', contact_form, name='contact_form'),
    path('contact/unread/', unread_contact_forms, name='unread_contact_forms'),
    path('disclaimer/', disclaimer, name='disclaimer'),
    path('beamer/login/', beamer_login_page, name='beamer_login_page'),
    path('beamer/password/', set_beamer_password, name='beamer_password'),
    path('beamer/lyrics/', beamer_lyrics, name='beamer_lyrics')
]
