"""
URL configuration
"""
import shared_django.accounts.urls
import shared_django.django_webauthn.urls
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('admin/', admin.site.urls),
    path('tehila/', include('tehila.urls')),
    path('', include('sites.urls')),
    path('', include('tehila.urls')),
    path('', include('sunset_foundation.urls')),
    path('webauthn/', include(shared_django.django_webauthn.urls)),
    path('accounts/', include(shared_django.accounts.urls)),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
