import logging
import time
import json
import traceback

logger = logging.getLogger('backend')


class LoggingMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        return self.get_response(request)

    def process_view(self, *_):
        return None

    def process_exception(self, request, exception):
        url = request.path_info
        try:
            user = request.user.pk
        except AttributeError:
            user = None
        try:
            session = request.session.session_key
        except AttributeError:
            session = None
        event = json.dumps({
            'severity': 'ERROR',
            'time': time.time(),
            'user': user,
            'url': url,
            'method': request.method,
            'session': session,
            'exception': {
                'message': str(exception),
                'type': type(exception).__name__,
                'traceback': traceback.format_tb(exception.__traceback__)
            },
        }, skipkeys=True)
        logger.error(event)
        return None