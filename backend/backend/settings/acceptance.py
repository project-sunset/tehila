
"""
Settings for the acceptance environment
"""
from django.core.files.storage import FileSystemStorage
from backend.settings.base import *
from .docker import *
try:
    from .config_acc import *
except ModuleNotFoundError:
    pass

ALLOWED_HOSTS = ['tehila-acc.projectsunset.nl']

WEBAUTHN = {
    'rp_id': 'tehila-acc.projectsunset.nl',
    'origin': 'https://tehila-acc.projectsunset.nl'
}

B2_STORAGE = FileSystemStorage(location=f'{MEDIA_ROOT}')
