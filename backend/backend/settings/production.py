"""
Settings for the production environment
"""
from .base import *
from shared_django.b2django.B2Storage import B2Storage
from .config_production import *
from .docker import *

ALLOWED_HOSTS = ['tehila.projectsunset.nl']

B2_STORAGE = B2Storage()

WEBAUTHN = {
    'rp_id': 'tehila.projectsunset.nl',
    'origin': 'https://tehila.projectsunset.nl'
}
