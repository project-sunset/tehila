B2 = {
    # Bucket to user for Storage
    'BUCKET_ID': '',
    'BUCKET_NAME': '',
    'APPLICATION_KEY_ID': '',
    'APPLICATION_KEY': '',
    'PUBLIC_URL': '',  # URL that points to bucket, for example: media.example.org
    # Bucket to use for DB backup
    'BACKUP_BUCKET_ID': '',
    'BACKUP_APPLICATION_KEY_ID': '',
    'BACKUP_APPLICATION_KEY': '',
}
SECRET_KEY = 'blablabla'
GOOGLE_CLIENT_ID = 'blabla'
