"""
Settings for development and testing
"""
from backend.settings.base import *
from django.core.files.storage import FileSystemStorage


SECRET_KEY = 'n8e)8@*%$p)j@w!-$m+2qwz(eqaxv+2x2!hx6j-l2&a*hq8rzy'
DEBUG = True
ALLOWED_HOSTS = ['*']
INTERNAL_IPS = ['localhost', '127.0.0.1']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        # This bug is hit without this option specified
        # https://github.com/python/cpython/issues/118172
        "OPTIONS": {
            'cached_statements': 0
        }
    }
}

FIXTURE_DIRS = ['sunset_foundation/fixtures', 'tehila/fixtures', 'shared_django/fixtures']

MIDDLEWARE = [
    'backend.dev_middleware.AddAccessControlAllowOrigin',
    'backend.dev_middleware.AlwaysReturnOkOnOPTIONS',
    'backend.dev_middleware.DisableCsrfInDrf',
] + [m for m in MIDDLEWARE if m != 'django.middleware.csrf.CsrfViewMiddleware']

B2_STORAGE = FileSystemStorage(location='local-upload')

STATIC_ROOT = 'static'

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}

MEDIA_ROOT = 'local-upload'

AUTH_PASSWORD_VALIDATORS = []

WEBAUTHN = {
    'rp_id': 'localhost',
    'origin': 'http://localhost:8080'
}
