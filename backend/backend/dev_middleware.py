from django.http import HttpResponse


class AddAccessControlAllowOrigin:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        response['Access-Control-Allow-Origin'] = 'http://localhost:8080'
        response['Access-Control-Allow-Headers'] = 'X-Csrftoken, content-type, x-tehila-band'
        response['Access-Control-Allow-Credentials'] = 'true'
        response['Access-Control-Allow-Methods'] = "GET, POST, PUT, DELETE, PATCH, OPTIONS, HEAD, patch"
        return response


class AlwaysReturnOkOnOPTIONS:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if request.method == 'OPTIONS':
            response = HttpResponse()
            response["Content-Length"] = 0
            return response
        return self.get_response(request)


class DisableCsrfInDrf:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        setattr(request, '_dont_enforce_csrf_checks', True)
        response = self.get_response(request)
        return response
