from django.http import HttpRequest
from rest_framework.permissions import BasePermission, SAFE_METHODS
from tehila.bands.models import Band


class IsInvitee(BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj.invitee.user == request.user


class IsOwner(BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj.user == request.user


class IsSuperuserOrReadOnly(BasePermission):
    def has_permission(self, request, view):
        return (
            request.method in SAFE_METHODS or
            request.user and request.user.is_superuser
        )


class IsCurrentBandOrChurchReadOnly(BasePermission):
    def has_object_permission(self, request, view, obj):
        if hasattr(request, 'tehila_band') and obj.band == request.tehila_band:
            return True
        elif request.method in SAFE_METHODS and obj.band.church == request.tehila_band.church:
            return True
        else:
            return False

# Unused, otherwise people can't edit tags


class SongPermissions(BasePermission):
    def has_object_permission(self, request: HttpRequest, view, obj):
        if obj.uuid is not None and not request.user.is_superuser and request.method not in SAFE_METHODS:
            return False
        return True

class HasBandSelected(BasePermission):
    message = 'Er is geen band geselecteerd'

    def has_permission(self, request, view):
        return hasattr(request, 'tehila_band')
