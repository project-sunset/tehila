import sqlite3

print('Inserting local webauth creds')

connection = sqlite3.connect('db.sqlite3')
cursor = connection.cursor()

user_id = cursor.execute("SELECT id FROM auth_user WHERE username='Bob' OR username='David' ORDER BY username").fetchone()[0]

try:
    cursor.execute(
        "INSERT INTO shared_django_webauthnuser (user_uuid, created, user_id) VALUES (?, ?, ?)",
        ('c61934ff85fe410e9beb54c956f24af7', '2023-08-03 17:23:27.084119', user_id)
    )
    connection.commit()
except sqlite3.IntegrityError:
    pass

webauthnuser = cursor.execute("SELECT id FROM shared_django_webauthnuser WHERE user_id=?", (user_id,)).fetchone()[0]

credential_public_key = bytes.fromhex("""
a401030339010020590100ce8c1646ad
c732de1dc0faf721e371a74242befbdf
48f649f0d01eafe269fd64c3f2a2cd96
b6e86439b95a23f249318815ad6d9ece
0b7271cc882ab004ceadc133c41ccfc2
ea613746e8e0457b9cef4af155c48d27
201e314190e9a21bd13f796a9c298f82
4cd807224fb031d45ddf3901d36af593
b32d29a8e7ffe0a09645d96c2a7fdf82
1c3856a2b71674afaea41dd1d6a20ef5
7f1f217d5ed370580fe0053513448222
b1c3430e85f7269921c08b238a9198a1
0fa2baea50ceed1e8af931cd33dbeeaa
a10844e0d232298875f9a1227761a1ea
983f6a7e669c9556a0c1702e1c960e28
47cf68b43891acd5ebb95a086404062e
0c2efba0f3b75ce54c8b792143010001""")

credential_id = bytes.fromhex("""
30949f23f4d6ea148ff1ca81ad73734d
8e3b9b9322256b01e0ec1dcb2e889865""")

cursor.execute(
    "INSERT INTO shared_django_webauthncred (credential_public_key, credential_id, current_sign_count, created, user_id) VALUES (?, ?, ?, ?, ?)",
    (credential_public_key, credential_id, 0, '2023-08-03 17:23:27.094475', webauthnuser)
)
connection.commit()
