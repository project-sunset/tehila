from backend.settings.local import FIXTURE_DIRS
import os
from datetime import date, timedelta

today = date.today()

REPLACEMENTS = {
    '[today]': today.isoformat(),
    '[tomorrow]': (today + timedelta(days=1)).isoformat(),
    '[last week]': (today - timedelta(days=7)).isoformat(),
}


def _get_fixture_filenames() -> list:
    files = []
    for dir in FIXTURE_DIRS:
        try:
            files.extend(map(lambda name, dir=dir: f'{dir}/raw/{name}', os.listdir(f'{dir}/raw')))
        except FileNotFoundError:
            pass
    return files


def set_fixture_dates():
    for filename in _get_fixture_filenames():
        with open(f'{filename}', 'r') as f:
            fixture = f.read()
        for date_tag, actual_date in REPLACEMENTS.items():
            fixture = fixture.replace(date_tag, actual_date)
        new_name = filename.replace('/raw', '')
        with open(new_name, 'w') as f:
            f.write(fixture)


if __name__ == '__main__':
    set_fixture_dates()
