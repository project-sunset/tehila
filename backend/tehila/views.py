from rest_framework import viewsets
from tehila.serializers import UpdateSerializer
from tehila.models import Update
from backend.permissions import IsSuperuserOrReadOnly
from rest_framework.pagination import LimitOffsetPagination

class UpdatePagination(LimitOffsetPagination):
    default_limit  = 3

class UpdateViewSet(viewsets.ModelViewSet):
    serializer_class = UpdateSerializer
    permission_classes = (IsSuperuserOrReadOnly,)
    queryset = Update.objects.order_by('-date')
    pagination_class = UpdatePagination
    