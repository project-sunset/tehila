from django.test import Client, TestCase


class BaseTestCase(TestCase):
    fixtures = [
        'users_test_data.json',
        'bands_test_data.json',
        'songs_test_data.json',
        'events_test_data.json',
        'recorder_test_data.json'
    ]


USERS = {
    'David': {
        'password': 'harp',
        'band': 1
    },
    'Jubal': {
        'password': 'lyre',
        'band': 2
    },
    'Solomon': {
        'password': 'psalm72'
    }
}


def client(name: str = None):
    def decorator(test_func):
        def wrapper(testcase):
            client = Client()
            if name is not None:
                client.login(username=name, password=USERS[name]['password'])
            test_func(testcase, client)
        return wrapper
    return decorator
