

from django.http import HttpRequest, HttpResponse

from tehila.bands.models import Band


def band_middleware(get_response):
    def middleware(request: HttpRequest):
        if 'x-tehila-band' in request.headers and request.user.is_authenticated:
            if not (type(request.headers['x-tehila-band']) is int or request.headers['x-tehila-band'].isdecimal()):
                return HttpResponse(f"{request.headers['x-tehila-band']} is niet een geldig band id", status=400, content_type='text/plain')
            try:
                request.tehila_band = Band.objects.get(pk=request.headers['x-tehila-band'], bandmembers__user=request.user)
            except Band.DoesNotExist:
                pass
        response = get_response(request)
        return response
    return middleware