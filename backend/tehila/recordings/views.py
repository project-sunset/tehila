from django.core.exceptions import PermissionDenied
from django.http import HttpResponseBadRequest, JsonResponse
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from backend.permissions import HasBandSelected
from tehila.bands.models import Band
from tehila.songs.models import Song

from .models import Recording
from .serializers import RecordingSerializer


class RecordingViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,HasBandSelected)
    serializer_class = RecordingSerializer
    pagination_class = None
    filter_fields = ('date',)

    def get_queryset(self):
        return Recording.objects.filter(bandsong__band=self.request.tehila_band)

    def perform_create(self, serializer):
        if serializer.validated_data['bandsong'].band.pk != self.request.tehila_band.pk:
            raise PermissionDenied
        serializer.save()

    @action(methods=['get'], detail=False, url_path='event')
    def recordings_for_a_event(self, request):
        event_id = request.GET.get('id')
        if event_id is None:
            return HttpResponseBadRequest("No event id specified")
        songs = Song.objects.filter(eventsong__event__pk=event_id)
        recordings = {}
        for song in songs:
            recordings[song.title] = RecordingSerializer(Recording.objects.filter(bandsong__song=song, bandsong__band=request.tehila_band).order_by('-date').first(), context={'request': request}).data
        return JsonResponse(recordings)
