from django.urls import include, path
from rest_framework import routers

from .views import RecordingViewSet

router = routers.SimpleRouter()
router.register('recordings', RecordingViewSet, basename='recording')


urlpatterns = [
    path('api/', include(router.urls)),
]
