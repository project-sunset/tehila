from django.conf import settings
from django.db import models, transaction
from tehila.bands.models import BandSong
from django.utils.text import slugify


def get_recording_name(instance, filename):
    band_uuid = instance.bandsong.band.uuid
    extension = filename[filename.rfind('.'):]
    recording_id = instance.id
    song_title_slug = slugify(instance.bandsong.song.title)
    return f'songs-audio/{band_uuid}/{recording_id}_{song_title_slug}{extension}'


class Recording(models.Model):
    bandsong = models.ForeignKey(BandSong, on_delete=models.CASCADE, related_name='recordings')
    audio = models.FileField(upload_to=get_recording_name, storage=settings.B2_STORAGE, null=True)
    date = models.DateField()
    public = models.BooleanField(default=False)
    description = models.CharField(max_length=200, null=True, blank=True)

    @transaction.atomic
    def save(self, *args, **kwargs):
        if self.id is not None:
            return super().save(*args, **kwargs)
        # If ID is not present, first save without file
        # then we can use the ID in the filename afterwards
        saved_file = self.audio
        self.audio = None
        super().save(*args, **kwargs)
        self.audio = saved_file
        return super().save(force_update=True)

    def __str__(self):
        return f"{self.date} - {self.bandsong}"
