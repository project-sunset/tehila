from rest_framework import serializers

from .models import Recording


class RecordingSerializer(serializers.ModelSerializer):
    song_title = serializers.CharField(source='bandsong.song.title', read_only=True)

    class Meta:
        model = Recording
        fields = ('id', 'audio', 'date', 'bandsong', 'description', 'song_title')

    wave_content_types = ['audio/wave', 'audio/wav', 'audio/x-wav', 'audio/x-pn-wav']
    valid_content_types = ['audio/ogg', 'audio/mpeg', 'audio/webm', 'audio/mp3']
    valid_extensions = ['.mp3', '.ogg', 'webm']

    def validate_audio(self, uploaded_file):
        if uploaded_file.content_type in self.wave_content_types:
            raise serializers.ValidationError('Wave files are not supported, use a compressed format.')
        elif uploaded_file.content_type not in self.valid_content_types:
            raise serializers.ValidationError('Content-Type "{}" is not supported'.format(uploaded_file.content_type))
        elif uploaded_file.name[-4:] not in self.valid_extensions:
            raise serializers.ValidationError('Extension not valid: {}'.format(uploaded_file.name[-4:]))
        elif uploaded_file.size > 20*1024*1024:
            raise serializers.ValidationError('File too big: {}mb, max 20mb'.format(uploaded_file.size/1024/1024))
        return uploaded_file
