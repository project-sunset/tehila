from .models import RecorderServer
from django.contrib import admin


@admin.register(RecorderServer)
class RecorderServerAdmin(admin.ModelAdmin):
    list_display = ('name', 'url')
