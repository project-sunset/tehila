from django.urls import include, path
from rest_framework import routers

from .views import (RecorderServerViewSet, delete_recording, get_band_event,
                    get_user_data, open_recorder, ping, save_recording,
                    search_song, create_song)

router = routers.SimpleRouter()
router.register('recorder-server', RecorderServerViewSet)

urlpatterns = [
    path('recorder/open/<str:recorder_id>/', open_recorder, name='open_recorder'),
    path('api/recorder/ping/', ping, name='ping'),
    path('api/recorder/user-data/', get_user_data, name='user_data'),
    path('api/recorder/search-song/', search_song, name='search_song'),
    path('api/recorder/save-recording/', save_recording, name='save_recording'),
    path('api/recorder/delete-recording/<str:recording_id>/', delete_recording, name='delete_recording'),
    path('api/recorder/band/<str:band_id>/event/', get_band_event, name='get_band_event'),
    path('api/recorder/song/', create_song, name="create_song"),
    path('api/', include(router.urls))
]
