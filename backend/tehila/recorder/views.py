from datetime import date, timedelta
from uuid import uuid4

from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.http import HttpRequest, HttpResponseRedirect
from django.utils import timezone
from rest_framework import viewsets
from rest_framework.decorators import (api_view, authentication_classes,
                                       permission_classes)
from rest_framework.permissions import IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response
from shared_django.accounts.models import ApiToken
from shared_django.CustomAuthBackends import TokenAuthentication
from tehila.bands.models import Band, BandMember, BandSong
from tehila.events.models import Event, EventSong
from tehila.events.serializers import SimpleEventSerializer
from tehila.recordings.models import Recording
from tehila.songs.models import Song
from tehila.songs.serializers import SongSerializer
from tehila.songs.song_search import find_matching_songs

from backend.permissions import IsSuperuserOrReadOnly

from .models import RecorderServer
from .serializers import RecorderServerSerializer


@login_required
def open_recorder(request: HttpRequest, recorder_id: str):
    server = RecorderServer.objects.get(pk=recorder_id)
    try:
        token = ApiToken.objects.get(user=request.user)
        if token.expiry > timezone.now() + timedelta(hours=2):
            return HttpResponseRedirect(f'{server.url}?token={token.token}')
        else:
            token.delete()
            new_token = ApiToken(user=request.user, token=uuid4(), expiry=timezone.now() + timedelta(hours=10))
            new_token.save()
            return HttpResponseRedirect(f'{server.url}?token={new_token.token}')
    except ApiToken.DoesNotExist:
        token = ApiToken(user=request.user, token=uuid4(), expiry=timezone.now() + timedelta(hours=10))
        token.save()
        return HttpResponseRedirect(f'{server.url}?token={token.token}')


@api_view(['GET'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def get_user_data(request: Request):
    bands = list(map(lambda b: {'name': b.name, 'id': b.id}, BandMember.objects.get(user=request.user).bands.all()))
    return Response({
        'username': request.user.username,
        'bands': bands
    })


@api_view(['POST'])
def ping(request: Request):
    try:
        server = RecorderServer.objects.get(name=request.data['server_name'])
        server.url = request.data['url']
        server.last_seen = timezone.now()
        server.save()
        return Response({'token': str(server.pk)})
    except RecorderServer.DoesNotExist:
        return Response(status=400, data={"message": "Unknown server name"})


@api_view(['GET'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def search_song(request: Request):
    search_term = request.GET.get('query')
    results, serializer = find_matching_songs(search_term)
    return Response({
        'count': len(results),
        'results': serializer.data})


@api_view(['POST'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
@transaction.atomic
def save_recording(request: Request):
    song = Song.objects.get(pk=request.POST.get('songId'))
    recording_file = request.FILES.get('recording')
    rec_date = date.fromisoformat(request.POST.get('date'))
    band = Band.objects.get(pk=request.POST.get('bandId'))
    description = None if request.POST.get('description') == '' else request.POST.get('description')
    bandsong, _ = BandSong.objects.get_or_create(band=band, song=song)
    recording = Recording(bandsong=bandsong, audio=recording_file, date=rec_date, description=description)
    recording.save()
    if 'eventId' in request.POST:
        event = Event.objects.get(pk=request.POST.get('eventId'), band=band)
        if not event.songs.filter(pk=song.pk).exists():
            event_song = EventSong(song=song, event=event, order=event.songs.count() + 1)
            event_song.save()
    return Response({'recordingId': recording.pk})

@api_view(['POST'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def create_song(request: Request):
    song = Song.objects.create(
        source=request.data['source'],
        title=request.data['title']
    )
    song.save()
    return Response(SongSerializer(song).data)


@api_view(['DELETE'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def delete_recording(_: Request, recording_id: str):
    try:
        Recording.objects.get(pk=recording_id).delete()
    except Recording.DoesNotExist:
        pass
    return Response(status=204)


@api_view(['GET'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def get_band_event(_: Request, band_id: str):
    band = Band.objects.get(pk=band_id)
    event = Event.objects.filter(
        band=band,
        date__gte=timezone.now()-timedelta(hours=10),
        date__lte=timezone.now()+timedelta(hours=5))
    if event.exists():
        event_data = SimpleEventSerializer(event[0])
        return Response(event_data.data)
    else:
        return Response(status=204)


class RecorderServerViewSet(viewsets.ModelViewSet):
    permission_classes = (IsSuperuserOrReadOnly,)
    serializer_class = RecorderServerSerializer
    pagination_class = None
    queryset = RecorderServer.objects.all()
