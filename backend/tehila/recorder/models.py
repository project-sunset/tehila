from django.db import models
from tehila.bands.models import Church


class RecorderServer(models.Model):
    church = models.ForeignKey(Church, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    url = models.URLField()
    last_seen = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.name
