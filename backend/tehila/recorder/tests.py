from datetime import timedelta
from uuid import uuid4

from django.contrib.auth.models import User
from django.test import tag
from django.test.client import Client
from django.utils import timezone
from shared_django.accounts.models import ApiToken
from tehila.utils_test import BaseTestCase, client


def get_token_headers():
    token = uuid4()
    ApiToken(user=User.objects.get(pk=1), token=token, expiry=timezone.now() + timedelta(hours=10)).save()
    return {'authorization': str(token)}


@tag('recorder')
class RecorderTests(BaseTestCase):

    @client('David')
    def test_token_creation(self, david: Client):
        response = david.get('/recorder/open/1/')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.headers['location'][0:28], "http://127.0.0.1:5173?token=")

    def test_unauthorized(self):
        response = self.client.get('/api/recorder/user-data/')
        self.assertEqual(response.status_code, 403)

    def test_user_data(self):
        token = get_token_headers()
        response = self.client.get('/api/recorder/user-data/', headers=token)
        self.assertEqual(response.status_code, 200)

    def test_ping(self):
        response = self.client.post('/api/recorder/ping/', {'server_name': 'LocalSpring', 'url': 'something'}, content_type='application/json')
        self.assertEqual(response.status_code, 200)

    def test_search_song(self):
        token = get_token_headers()
        response = self.client.get('/api/recorder/search-song/?query=bla', headers=token)
        self.assertEqual(response.status_code, 200)

    def test_band_event(self):
        token = get_token_headers()
        response = self.client.get('/api/recorder/band/1/event/', headers=token)
        self.assertEqual(response.status_code, 204)
