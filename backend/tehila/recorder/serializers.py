
from rest_framework import serializers

from .models import RecorderServer


class RecorderServerSerializer(serializers.ModelSerializer):
    class Meta:
        model = RecorderServer
        fields = ('id', 'name', 'url', 'last_seen')
