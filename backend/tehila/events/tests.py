from datetime import datetime

from django.test import tag
from django.test.client import Client
from tehila.events.ics import ICS
from tehila.utils_test import BaseTestCase, client


@tag('events')
class EventsTests(BaseTestCase):

    @client('David')
    def test_fix_event_order(self, david: Client):
        response = david.patch('/api/events/1/fix-order/', headers={'x-tehila-band': 1})
        self.assertEqual(200, response.status_code)
        self.assertTrue(response.json()['fixed'])

    def test_ics(self):
        ics = ICS('ICS TEST')
        event_1 = {
            'last_modified': datetime(2022, 1, 1, 14, 0, 0),
            'start': datetime(2022, 2, 2, 14, 0, 0),
            'duration_in_minutes': 90,
            'location': 'Oostpoort',
            'summary': 'Oefenen Test'
        }
        ics.add_event(event_1)
        event_2 = {
            'last_modified': datetime(2022, 1, 1, 14, 0, 0),
            'start': datetime(2022, 2, 3, 15, 0, 0),
            'duration_in_minutes': 60,
            'location': 'Oostpoort',
            'summary': 'Dienst Test'
        }
        ics.add_event(event_2)
        calendar = ics.get_ics()
        with open('tehila/fixtures/test.ics', 'r', newline='') as f:
            expected_calendar = f.read()
        self.assertEqual(expected_calendar, calendar)

    @client()
    def test_old_ics_endpoint(self, client: Client):
        response = client.get('/tehila/calendar/7465cd1a-28c9-4ecc-9ee8-a6ec7ab855b7.ics')
        self.assertEqual(200, response.status_code)
        self.assertContains(response, 'SUMMARY:Original Psalm Project - Kerkdienst', 1)

    @client()
    def test_services_ics_endpoint(self, client: Client):
        response = client.get('/tehila/calendar/7465cd1a-28c9-4ecc-9ee8-a6ec7ab855b7-services.ics')
        self.assertEqual(200, response.status_code)
        self.assertContains(response, 'SUMMARY:Original Psalm Project - Kerkdienst', 1)

    @client()
    def test_new_ics_endpoint(self, client: Client):
        response = client.get('/calendar/7465cd1a-28c9-4ecc-9ee8-a6ec7ab855b7.ics')
        self.assertEqual(200, response.status_code)
        self.assertContains(response, 'SUMMARY:Original Psalm Project - Kerkdienst', 1)
