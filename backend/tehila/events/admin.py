from django.contrib import admin

from .models import Event, EventSong


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    list_display = ('date', 'band')


@admin.register(EventSong)
class EventSongAdmin(admin.ModelAdmin):
    list_display = ('event', 'song', 'order')
