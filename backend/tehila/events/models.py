from django.db import models
from tehila.bands.models import Band
from tehila.songs.models import OpenLyrics, PdfMusic, Song
from django.core.exceptions import ValidationError

class Event(models.Model):
    SERVICE = 'SERVICE'
    PRACTICE = 'PRACTICE'
    EVENT_TYPES = [
        (SERVICE, 'Service'),
        (PRACTICE, 'Practice')
    ]
    preparation_start = models.DateTimeField(null=True, blank=True)
    date = models.DateTimeField()
    band = models.ForeignKey(Band, on_delete=models.CASCADE)
    event_type = models.CharField(max_length=8, choices=EVENT_TYPES, default=SERVICE)
    songs = models.ManyToManyField(Song, through='EventSong')
    description = models.TextField(null=True, blank=True)
    link = models.URLField(null=True, blank=True)
    last_modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"event {self.band} - {self.date}"


class EventSong(models.Model):
    song = models.ForeignKey(Song, on_delete=models.PROTECT, related_name='eventsong')
    event = models.ForeignKey(Event, on_delete=models.CASCADE, related_name='songlist')
    order = models.IntegerField()
    openlyrics = models.ForeignKey(OpenLyrics, on_delete=models.SET_NULL, null=True, blank=True)
    pdfmusic = models.ForeignKey(PdfMusic, on_delete=models.SET_NULL, null=True, blank=True)

    def clean(self):
        if self.openlyrics is not None and self.pdfmusic is not None:
            raise ValidationError("Openlyrics and PDF music should not both be set")

    def __str__(self):
        return f'{self.song} - {self.event}'