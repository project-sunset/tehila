import io
import zipfile
from datetime import timedelta

from django.core.cache import cache
from django.http import HttpResponseRedirect
from django.http.response import HttpResponse, JsonResponse
from django.utils import timezone
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.pagination import Cursor, CursorPagination
from rest_framework.permissions import IsAuthenticated
from backend.permissions import HasBandSelected
from tehila.bands.models import Band
from tehila.songs import chordpro

from .ics import ICS, IcsEvent
from .models import Event, EventSong
from .serializers import (EventSerializer, EventSongSerializer,
                          SimpleEventSerializer)


class EventPagination(CursorPagination):
    page_size = 10
    ordering = 'date'


class EventViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,HasBandSelected)
    serializer_class = EventSerializer
    pagination_class = EventPagination
    filter_fields = ('date',)

    def get_serializer_class(self):
        if self.action == 'list':
            return SimpleEventSerializer
        return EventSerializer

    def get_queryset(self):
        return Event.objects.filter(band=self.request.tehila_band)

    def perform_create(self, serializer):
        serializer.save(band=self.request.tehila_band)

    @action(methods=['get'], detail=False, url_path='get-cursor')
    def get_cursor(self, request):
        # This hijacks some DRF internals to generate a cursor that starts at a given time
        yesterday = (timezone.now() - timedelta(days=1)).isoformat(timespec='seconds')
        cursor = Cursor(offset=0, reverse=False, position=[yesterday])
        self.paginator.base_url = request.build_absolute_uri()
        start_url = self.paginator.encode_cursor(cursor)
        return HttpResponseRedirect(start_url.replace('/get-cursor', ''))

    @action(methods=['patch'], detail=True, url_path='fix-order')
    def fix_order(self, request, pk):
        event = self.get_queryset().get(pk=pk)
        ordered_songlist = EventSong.objects.filter(event=event).order_by('order')
        fixed = False
        for i in range(0, len(ordered_songlist)):
            if ordered_songlist[i].order != i:
                fixed = True
                ordered_songlist[i].order = i
                ordered_songlist[i].save()
        return JsonResponse({'fixed': fixed})

    @action(methods=['get'], detail=False, url_path='latest')
    def latest_events(self, request):
        compare_time = timezone.now()-timedelta(hours=5)
        next_event = self.get_queryset().filter(date__gte=compare_time).order_by('date').first()
        last_event = self.get_queryset().filter(date__lt=compare_time).order_by('-date').first()
        response = []
        if last_event is not None:
            serializer = SimpleEventSerializer(last_event)
            response.append(serializer.data)
        else:
            response.append(None)
        if next_event is not None:
            serializer = SimpleEventSerializer(next_event)
            response.append(serializer.data)
        else:
            response.append(None)
        return JsonResponse(response, safe=False)

    # Override the permission class because I want to use an anchor tag in HTML
    # which means I can't send the x-tehila-band header.
    # maybe a better design puts the band in the URL path.
    @action(methods=['get'], detail=True, permission_classes=[IsAuthenticated])
    def chordpro(self, request, pk):
        event = Event.objects.get(pk=pk)
        data = io.BytesIO()
        with zipfile.ZipFile(data, mode='w') as f:
            event_songs = EventSong.objects.filter(event=event).order_by('order')
            for i, song in enumerate(event_songs, start=1):
                if song.openlyrics is not None:
                    f.writestr(f'{i} {song.song.title}.cho', chordpro.openlyrics_to_chordpro(song.song.title, song.openlyrics.content))
        response = HttpResponse(data.getvalue(), content_type='application/zip')
        response['Content-Disposition'] = 'attachment; filename=chordpro_event.zip'
        return response

            


class EventSongViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,HasBandSelected)
    pagination_class = None
    serializer_class = EventSongSerializer

    def get_queryset(self):
        return EventSong.objects.filter(event__band=self.request.tehila_band)

def _get_calendar(band: Band, event_type=None):
    cache_key = f'{band.uuid}-${event_type}-ics'
    cached_data = cache.get(cache_key)
    if cached_data is not None:
        return cached_data
    events = Event.objects.filter(band=band, date__gte=timezone.now()-timedelta(days=30))
    if event_type is not None:
        events = events.filter(event_type=event_type)
    ics = ICS(band.name)
    for event in events:
        event_start = event.preparation_start if event.preparation_start is not None else event.date
        ics_event: IcsEvent = {
            'last_modified': event.last_modified,
            'start': event_start,
            'duration_in_minutes': _get_event_duration(event),
            'location': 'Oostpoort',
            'summary': _get_event_summary(band.name, event),
            'description': event.description
        }
        ics.add_event(ics_event)
    ics_file = ics.get_ics()
    cache.set(cache_key, ics_file, 7*24*60*60)
    return ics_file

def get_calendar(_, band_uuid):
    band = Band.objects.get(uuid=band_uuid)
    return HttpResponse(
        _get_calendar(band), 
        content_type='text/calendar', 
        headers={'Content-Disposition': f'attachment; filename="{band.name}.ics"'})

def get_services_calendar(_, band_uuid):
    band = Band.objects.get(uuid=band_uuid)
    return HttpResponse(
        _get_calendar(band, event_type=Event.SERVICE), 
        content_type='text/calendar', 
        headers={'Content-Disposition': f'attachment; filename="{band.name}-services.ics"'})

def _get_event_duration(event: Event):
    base = 180 if event.event_type == Event.PRACTICE else 90
    if event.preparation_start is None:
        return base
    delta = event.date - event.preparation_start
    return base + round(delta / timedelta(minutes=1))


def _get_event_summary(band_name: str, event: Event):
    if event.event_type == Event.PRACTICE:
        return f'{band_name} - Oefenen'
    else:
        return f'{band_name} - Kerkdienst'
