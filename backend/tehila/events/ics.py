from typing import TypedDict
from datetime import datetime


class IcsEvent(TypedDict):
    last_modified: datetime
    start: datetime
    duration_in_minutes: int
    location: str
    summary: str
    description: str


class ICS:
    # See https://datatracker.ietf.org/doc/html/rfc5545
    def __init__(self, calendar_name):
        self.calendar_name = calendar_name
        self.events = []
        self.timezone = 'TZID=Europe/Amsterdam'
        self.dt_format = "%Y%m%dT%H%M%S"
        self.ics_str = ''

    def get_ics(self):
        self.ics_str = ''
        self._add_content_line("BEGIN:VCALENDAR")
        self._add_content_line("PRODID:Tehila")
        self._add_content_line("VERSION:2.0")
        self._add_content_line(f"NAME:{self.calendar_name}")
        for event in self.events:
            self._add_vevent(event)
        self._add_content_line("END:VCALENDAR")
        return self.ics_str

    def add_event(self, event: IcsEvent):
        self.events.append(event)

    def _add_vevent(self, event: IcsEvent):
        self._add_content_line("BEGIN:VEVENT")
        self._add_content_line(f"UID:{self.calendar_name}-{event['start'].strftime(self.dt_format)}@tehila.projectsunset.nl")
        self._add_content_line(f"DTSTAMP:{event['last_modified'].strftime(self.dt_format)}")
        self._add_content_line(self._tz_datetime("DTSTART", event['start']))
        self._add_content_line(f"DURATION:PT{event['duration_in_minutes']}M")
        self._add_content_line(f"LOCATION:{event['location']}")
        if self._attribute_present(event, "summary"):
            self._add_content_line(f'SUMMARY:{event["summary"]}')
        if self._attribute_present(event, 'description'):
            self._add_content_line(f'DESCRIPTION:{event["description"]}')
        self._add_content_line("END:VEVENT")

    @staticmethod
    def _attribute_present(event: IcsEvent, key: str):
        return key in event and event[key] is not None and event[key] != ''

    def _add_content_line(self, line: str):
        while len(line) > 75:
            self.ics_str += self._add_content_line(line[:75]) + " "
            line = line[75:]
        self.ics_str += line + "\r\n"

    def _tz_datetime(self, property: str, dt: datetime):
        dt_string = dt.strftime(self.dt_format)
        return f"{property};{self.timezone}:{dt_string}"
