from django.urls import include, path
from rest_framework import routers

from .views import EventSongViewSet, EventViewSet, get_calendar, get_services_calendar

router = routers.SimpleRouter()
router.register('events', EventViewSet, basename='event')
router.register('eventsong', EventSongViewSet, basename='eventsong')

urlpatterns = [
    path('api/', include(router.urls)),
    path('calendar/<str:band_uuid>-services.ics', get_services_calendar),
    path('calendar/<str:band_uuid>.ics', get_calendar),
]
