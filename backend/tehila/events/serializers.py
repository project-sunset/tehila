from rest_framework import serializers
from tehila.songs.models import Song
from tehila.songs.serializers import SongSerializer

from .models import Event, EventSong

DATE_TIME_FORMAT = "%Y-%m-%dT%H:%M"


class EventSongSerializer(serializers.ModelSerializer):
    song = SongSerializer()

    class Meta:
        model = EventSong
        fields = ('id', 'song', 'order', 'event', 'openlyrics', 'pdfmusic')

    def create(self, validated_data):
        validated_data['song'] = Song.objects.get(pk=self.initial_data['song']['id'])
        return super().create(validated_data)

    def update(self, instance, validated_data):
        if not self.partial:
            validated_data['song'] = Song.objects.get(pk=self.initial_data['song']['id'])
        return super().update(instance, validated_data)


class SimpleEventSerializer(serializers.ModelSerializer):
    date = serializers.DateTimeField(format=DATE_TIME_FORMAT)
    preparation_start = serializers.DateTimeField(format=DATE_TIME_FORMAT)

    class Meta:
        model = Event
        fields = ('id', 'date', 'description', 'link', 'event_type', 'preparation_start')


class EventSerializer(serializers.ModelSerializer):
    date = serializers.DateTimeField(format=DATE_TIME_FORMAT)
    preparation_start = serializers.DateTimeField(format=DATE_TIME_FORMAT, allow_null=True, default=None)
    songlist = EventSongSerializer(read_only=True, many=True)

    def validate(self, data):
        if data['preparation_start'] is not None:
            if data['date'] <= data['preparation_start']:
                raise serializers.ValidationError('"Band eerder aanwezig" mag niet later zijn dan de event datum')
            if data['date'].date() != data['preparation_start'].date():
                raise serializers.ValidationError('"Band eerder aanwezig" moet op dezelfde dag zijn als het event')
        return data

    class Meta:
        model = Event
        fields = ('id', 'date', 'songlist', 'description', 'link', 'event_type', 'preparation_start')
