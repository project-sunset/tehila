from django.core.cache import cache
from django.db.models.signals import m2m_changed, post_delete, post_save, pre_delete
from django.dispatch import receiver

from tehila.events.models import Event
from tehila.songs.models import Collection, Song, SongInCollection, Tag
from tehila.bands.models import BandSong, Band


@receiver(post_save, sender=Tag)
@receiver(post_save, sender=Song)
@receiver(post_save, sender=Collection)
@receiver(post_save, sender=Event)
@receiver(post_save, sender=SongInCollection)
@receiver(post_save, sender=BandSong)
@receiver(post_delete, sender=Tag)
@receiver(post_delete, sender=Song)
@receiver(post_delete, sender=Collection)
@receiver(post_delete, sender=Event)
@receiver(post_delete, sender=SongInCollection)
@receiver(post_delete, sender=BandSong)
@receiver(m2m_changed, sender=Event.songs.through)
@receiver(m2m_changed, sender=Song.tags.through)
@receiver(m2m_changed, sender=Collection.songs.through)
def invalidate_repertoire_caches(sender, **kwargs):
    bands = Band.objects.all()
    cache.delete_many(map(lambda band: f'{band.id}-repertoire', bands))


@receiver(post_save, sender=Event)
@receiver(pre_delete, sender=Event)
def invalidate_ics_caches(sender, **kwargs):
    try:
        band = kwargs['instance'].band
        cache.delete(f'{band.uuid}-ics')
    except Band.DoesNotExist:
        pass  # Happens during loading of data
