from django.urls import include, path
from rest_framework import routers
from .views import UpdateViewSet

router = routers.SimpleRouter()
router.register('updates', UpdateViewSet)

urlpatterns = [
    path('api/', include(router.urls)),
    path('', include('tehila.bands.urls')),
    path('', include('tehila.recordings.urls')),
    path('', include('tehila.events.urls')),
    path('', include('tehila.songs.urls')),
    path('', include('tehila.recorder.urls'))
]
