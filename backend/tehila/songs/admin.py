from django.contrib import admin

from .models import Collection, Song, SongInCollection, Tag, OpenLyrics, UserOpenLyricsSettings, Lyrics, PdfMusic, PdfPage


@admin.register(UserOpenLyricsSettings)
class UserOpenLyricsSettingsAdmin(admin.ModelAdmin):
    pass


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = ('name', 'background_color', 'text_color')


@admin.register(Song)
class SongAdmin(admin.ModelAdmin):
    list_display = ('title', 'release_year')


@admin.register(Collection)
class CollectionAdmin(admin.ModelAdmin):
    list_display = ('title',)


@admin.register(SongInCollection)
class SongInCollectionAdmin(admin.ModelAdmin):
    list_display = ('song', 'collection', 'number')


@admin.register(OpenLyrics)
class OpenLyricsAdmin(admin.ModelAdmin):
    list_display = ('song', 'band', 'name')


@admin.register(Lyrics)
class LyricsAdmin(admin.ModelAdmin):
    list_display = ('song',)

@admin.register(PdfMusic)
class PdfMusicAdmin(admin.ModelAdmin):
    list_display = ('song',)

@admin.register(PdfPage)
class PdfPageAdmin(admin.ModelAdmin):
    list_display = ('pdfmusic','page_number')