from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import tag
from django.test.client import Client
from tehila.utils_test import BaseTestCase, client


@tag('songs')
class SongsTests(BaseTestCase):

    @client('David')
    def test_manage_tags_add(self, david: Client):
        response = david.post('/api/songs/3/tags/', {
            'tagId': 1,
            'action': 'add'
        }, content_type='application/json')
        self.assertEqual(204, response.status_code)
        response = david.get('/api/songs/3/')
        self.assertEqual(200, response.status_code)
        self.assertEqual(1, len(response.json()['tags']))
        self.assertEqual('psalm', response.json()['tags'][0]['name'])

    @client('David')
    def test_manage_tags_remove(self, david: Client):
        response = david.post('/api/songs/1/tags/', {
            'tagId': 1,
            'action': 'delete'
        }, content_type='application/json')
        self.assertEqual(204, response.status_code)
        response = david.get('/api/songs/1/')
        self.assertEqual(200, response.status_code)
        self.assertEqual(0, len(response.json()['tags']))

    @client('Jubal')
    def test_get_songs_in_collection(self, jubal: Client):
        response = jubal.get('/api/collections/1/songs/')
        self.assertEqual(200, response.status_code)
        self.assertEqual(2, len(response.json()['data']))

    @client('Jubal')
    def test_get_songs_in_no_collection(self, jubal: Client):
        response = jubal.get('/api/collections/null/songs/')
        self.assertEqual(200, response.status_code)
        self.assertEqual(1, len(response.json()['data']))

    @client()
    def test_song_search(self, client: Client):
        response = client.get('/api/songs/search/?query=garden')
        self.assertEqual(200, response.status_code)
        self.assertEqual('Garden of Eden', response.json()['results'][0]['title'])
    
    @client()
    def test_song_search_number(self, client: Client):
        response = client.get('/api/songs/search/?query=1')
        self.assertEqual(200, response.status_code)
        self.assertEqual('Psalm 1', response.json()['results'][0]['title'])
    
    @client()
    def test_song_search_collection_and_number(self, client: Client):
        response = client.get('/api/songs/search/?query=Psal 3')
        self.assertEqual(200, response.status_code)
        self.assertEqual('Unwritten Psalm', response.json()['results'][0]['title'])

    @client()
    def test_song_text_search(self, client: Client):
        response = client.get('/api/songs/text-search/?query=text of psalm 1')
        self.assertEqual(200, response.status_code)
        self.assertEqual('Psalm 1', response.json()['results'][0]['title'])

    @client('David')
    def test_openlyrics_import(self, david: Client):
        with open('tehila/songs/openlyrics_test.xml', 'rb') as f:
            xml_file = SimpleUploadedFile('openlyrics_test.xml', f.read(), content_type='text/xml')
            response = david.post('/api/import-openlyrics/', {
                'file': xml_file
            })
        self.assertEqual(200, response.status_code, msg=response.content)
        self.assertEqual(1, response.json()['input_files'])
        self.assertEqual(1, response.json()['songs_added'])
