from os import listdir
from subprocess import run
from tempfile import TemporaryDirectory

from django.core.files import File
from django.core.files.uploadedfile import UploadedFile
from django.db import transaction
from tehila.bands.models import Band
from tehila.songs.models import PdfMusic, PdfPage, Song


@transaction.atomic
def pdf2images(uploaded_pdf: UploadedFile, band: Band, song_id: int, name: str) -> PdfMusic:
    with TemporaryDirectory(ignore_cleanup_errors=True) as folder:
        run(['pdftocairo', '-png', '-r', '150', '-', 'output'], input=uploaded_pdf.read(), cwd=folder, check=True)
        run(['mogrify',  '-trim', '*.png'], cwd=folder, check=True)
        pdf_music = PdfMusic.objects.create(
            band=band,
            song=Song.objects.get(pk=song_id),
            name=name
        )
        pdf_music.save()
        for page in listdir(folder):
            page_number = int(page[7:-4])
            pdf_page = PdfPage.objects.create(
                pdfmusic=pdf_music,
                page_number=page_number,
            )
            pdf_page.save()
            with open(f'{folder}/{page}', 'rb') as image_file:
                pdf_page.image.save(page, File(image_file))
        return pdf_music