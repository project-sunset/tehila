import re
from typing import TypedDict
import xml.etree.ElementTree as ET

from django.core.exceptions import ValidationError
from tehila.songs.models import Collection, Lyrics, Song, SongInCollection

NS = '{http://openlyrics.info/namespace/2009/song}'


def process_openlyrics(raw_xml: str, filename: str, known_collections):
    root = ET.fromstring(raw_xml)

    # Find existing song, or create new
    comment = root.find(f'.//{NS}comment')
    if comment is None:
        raise ValidationError(f'File {filename} has no UUID comment')
    defaults = {'title': root.find(f'.//{NS}title').text}
    release_year = root.find(f'.//{NS}released')
    if release_year is not None:
        defaults['release_year'] = release_year.text
    song, song_created = Song.objects.update_or_create(
        uuid=comment.text[5:],
        defaults=defaults
    )

    # Update collections
    songbooks = root.findall(f'.//{NS}songbook')
    seen_sic_pks = []
    sics_created = 0
    for songbook in songbooks:
        collection_title = songbook.get('name')
        if collection_title not in known_collections:
            raise ValidationError(f'File {filename} contains a collection "{collection_title}" does not exist in Tehila')
        number = songbook.get('entry')
        collection = Collection.objects.get(title=collection_title)
        sic, sic_created = SongInCollection.objects.update_or_create(
            song=song,
            collection__title=collection_title,
            defaults={'number': number, 'collection': collection})
        seen_sic_pks.append(sic.collection.pk)
        if sic_created:
            sics_created += 1
    sic_deleted_count, _ = SongInCollection.objects.filter(song=song).exclude(collection__pk__in=seen_sic_pks).delete()

    # Update lyrics
    raw_lyrics = get_content(root)['cmp_text']
    if raw_lyrics is not None:
        _, lyrics_created = Lyrics.objects.update_or_create(
            song=song,
            defaults={
                'xml': raw_xml,
                'plain': raw_lyrics
            }
        )
    else:
        lyrics_created = False

    return song_created, sics_created, sic_deleted_count, lyrics_created

class Content(TypedDict):
    verse_order: list[str]
    verses: dict[str, list[list[str]]]
    cmp_text: str

def get_content(root: ET.Element, cmp=True):
    if root.find(f'.//{NS}verseOrder') is None:
        return None
    verse_order = root.find(f'.//{NS}verseOrder').text.split(' ')
    content: Content = {
        'verse_order': verse_order,
        'verses': {},
        'cmp_text': ''
    }
    for verse_element in root.findall(f".//{NS}verse"):
        verse = []
        for lines in verse_element.findall(f'{NS}lines'):
            if lines.text is not None:
                verse.append(re.sub(r'\s+', ' ', lines.text.strip()))
            for br in lines.findall(f'.//{NS}br'):
                verse.append(re.sub(r'\s+', ' ', br.tail.strip()))
        content['verses'][verse_element.get('name')] = verse
    if cmp:
        for verse_name in verse_order:
            if verse_name == 'm':
                continue
            content['cmp_text'] += "\n".join(content['verses'][verse_name]) + '\n\n'
        content['cmp_text'] = normalize_string(content['cmp_text'])
    return content


UNIDECODE = {
    'ë': 'e',
    'ï': 'i',
    'ä': 'a',
    'ü': 'u'
}
DELETE_CHARS = {',', '.', "'", '(', ')', 'é', '!', '?', ':', '’', ';', '…', '/'}


def normalize_string(string: str):
    new_string = string.lower()
    # Delete punctuation
    for char in DELETE_CHARS:
        new_string = new_string.replace(char, '')
    # Only ASCII stuff
    for key, value in UNIDECODE.items():
        new_string = new_string.replace(key, value)
    # No new lines
    new_string = new_string.replace('\n', ' ')
    new_string = new_string.replace('  ', ' ')
    return new_string

VERSE_NAME_MAPPING = {
    'i': 'intro',
    'v': 'vers',
    'p': 'prechorus',
    'c': 'refrein',
    's': 'solo',
    'b': 'bridge',
    'o': 'overig',
    'm': 'instrumentaal',
    'e': 'einde'
}

def long_verse_name(name: str):
    letter = name[0]
    longform = VERSE_NAME_MAPPING[letter]
    if len(name) == 1:
        return longform
    else:
        return f'{longform} {name[1:]}'