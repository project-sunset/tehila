
import zipfile
from operator import itemgetter

from django.contrib.auth.decorators import user_passes_test
from django.core.exceptions import PermissionDenied, ValidationError
from django.db import transaction
from django.db.models import Count, Q
from django.http import HttpRequest, HttpResponse, JsonResponse
from rest_framework import mixins, viewsets
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from tehila.bands.models import Band
from tehila.events.models import Event
from tehila.songs.chordpro import openlyrics_to_chordpro
from tehila.songs.lyrics_processor import process_openlyrics
from tehila.songs.musicxml_to_openlyrics import musicxml_to_openlyrics
from tehila.songs.pdf2images import pdf2images
from tehila.songs.song_search import find_matching_songs

from backend.permissions import (HasBandSelected,
                                 IsCurrentBandOrChurchReadOnly, IsOwner,
                                 IsSuperuserOrReadOnly)

from .models import (Collection, Lyrics, OpenLyrics, PdfMusic, Song,
                     SongInCollection, Tag, UserOpenLyricsSettings)
from .serializers import (CollectionSerializer, LyricsSerializer,
                          OpenLyricsNoContentSerializer, OpenLyricsSerializer, PdfMusicNoNestedSerializer, PdfMusicSerializer,
                          SongInCollectionSerializer, SongSerializer,
                          TagSerializer, UserOpenLyricsSettingsSerializer)


class UserOpenLyricsSettingsViewSet(viewsets.ModelViewSet):
    permission_classes = (IsOwner,)
    serializer_class = UserOpenLyricsSettingsSerializer
    pagination_class = None
    filter_fields = ('openlyrics',)

    def get_queryset(self):
        return UserOpenLyricsSettings.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    @action(methods=['get'], detail=False, url_path='event')
    def event_settings(self, request):
        event = Event.objects.get(id=request.GET.get('event'))
        db_data = self.get_queryset().filter(openlyrics__in=OpenLyrics.objects.filter(eventsong_set__event=event))
        serializer = UserOpenLyricsSettingsSerializer(data=db_data, many=True)
        serializer.is_valid()
        return JsonResponse({'settings': serializer.data})


class OpenLyricsViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, IsCurrentBandOrChurchReadOnly, HasBandSelected)
    pagination_class = None
    filter_fields = ('song',)

    def get_serializer_class(self):
        if self.action == 'list':
            return OpenLyricsNoContentSerializer
        return OpenLyricsSerializer

    def get_queryset(self):
        return OpenLyrics.objects.filter(band__church=self.request.tehila_band.church)

    # Override the permission class because I want to use an anchor tag in HTML
    # which means I can't send the x-tehila-band header.
    # maybe a better design puts the band in the URL path.
    @action(methods=["get"], detail=True, permission_classes=[IsAuthenticated])
    def chordpro(self, request, pk):
        openlyrics = OpenLyrics.objects.get(pk=pk)
        response = HttpResponse(openlyrics_to_chordpro(openlyrics.song.title, openlyrics.content), content_type="text/plain")
        response.headers["Content-Disposition"] = f'attachment; filename={openlyrics.song.title}.cho'
        return response

class PdfMusicViewSet(mixins.ListModelMixin,
                    mixins.RetrieveModelMixin,
                    mixins.DestroyModelMixin,
                    viewsets.GenericViewSet
                    ):
    permission_classes = (IsAuthenticated, IsCurrentBandOrChurchReadOnly, HasBandSelected)
    pagination_class = None
    filter_fields = ('song',)

    def get_serializer_class(self):
        if self.action == 'list':
            return PdfMusicNoNestedSerializer
        return PdfMusicSerializer
    
    def get_queryset(self):
        return PdfMusic.objects.filter(band__church=self.request.tehila_band.church)

    @action(methods=['post'], detail=False)
    def upload(self, request: HttpRequest):
        pdf_file = request.FILES['0']
        if pdf_file.content_type != 'application/pdf':
            raise ValidationError(f'File {pdf_file.name} does not have content-type application/pdf')
        if not hasattr(request, 'tehila_band'):
            raise ValidationError('No band selected')
        if 'name' not in request.GET or len(request.GET['name']) == 0:
            raise ValidationError("No name provided")
        pdf_music = pdf2images(pdf_file, request.tehila_band, request.GET['song'], request.GET['name'])
        return Response(PdfMusicSerializer(pdf_music).data)

class TagViewSet(viewsets.ModelViewSet):
    permission_classes = (IsSuperuserOrReadOnly,)
    serializer_class = TagSerializer
    pagination_class = None
    queryset = Tag.objects.all()


class LyricsViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = LyricsSerializer
    filter_fields = ('song',)
    queryset = Lyrics.objects.all()

class SongViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = SongSerializer
    filter_fields = ('title', 'release_year', 'song_in_collection__number')
    queryset = Song.objects.all()

    def create(self, request, *args, **kwargs):
        if 'collections' in request.data and not request.user.is_superuser:
            raise PermissionDenied('Not allowed to create song with collection')
        return super().create(request, *args, **kwargs)

    @action(methods=["post"], detail=True)
    def tags(self, request, pk):
        if request.data['action'] == 'add':
            Song.objects.get(id=pk).tags.add(Tag.objects.get(id=request.data['tagId']))
        elif request.data['action'] == 'delete':
            Song.objects.get(id=pk).tags.remove(Tag.objects.get(id=request.data['tagId']))
        return HttpResponse(status=204)

    @action(methods=['get'], detail=False, url_path='search', permission_classes=[])
    def song_search(self, request):
        search_term = request.GET.get('query')
        results, serializer = find_matching_songs(search_term)
        return JsonResponse({
            'count': len(results),
            'results': serializer.data})

    @action(methods=['get'], detail=False, url_path='text-search', permission_classes=[])
    def text_search(self, request):
        search_term = request.GET.get('query')
        songs = Song.objects.filter(text__icontains=search_term)
        serializer = SongSerializer(data=songs, many=True)
        serializer.is_valid()
        return JsonResponse({'results': serializer.data})


class CollectionViewSet(viewsets.ModelViewSet):
    permission_classes = (IsSuperuserOrReadOnly,)
    serializer_class = CollectionSerializer
    pagination_class = None
    filter_fields = ('title',)
    queryset = Collection.objects.all()

    @action(methods=['get'], detail=True)
    def songs(self, request, pk):
        if pk == 'null':
            songs = Song.objects.annotate(num_collections=Count('song_in_collection')).filter(num_collections=0)
        else:
            songs = Song.objects.filter(song_in_collection__collection__id=pk)
        response = []
        for song in songs:
            if pk == 'null':
                response.append({
                    'id': song.id,
                    'title': song.title,
                    'number': None
                })
            else:
                collection = song.song_in_collection.get(collection__id=pk)
                response.append({
                    'id': song.id,
                    'title': song.title,
                    'number': collection.number
                })
        if pk != 'null':
            response.sort(key=_determine_collection_sort_key)
        return JsonResponse({'data': response})


def _determine_collection_sort_key(s):
    if s['number'] is None or s['number'] == '':
        return 0
    if s['number'].isnumeric():
        return int(s['number'])
    # assume something like 85a
    return int(s['number'][:-1])


class SongInCollectionViewSet(viewsets.ModelViewSet):
    permission_classes = (IsSuperuserOrReadOnly,)
    serializer_class = SongInCollectionSerializer
    pagination_class = None
    filter_fields = ('number', 'song', 'collection')
    queryset = SongInCollection.objects.all()

    def perform_create(self, serializer):
        serializer.save()  # Otherwise DRF will complain about a duplicate title with Collection


@user_passes_test(lambda u: u.is_superuser)
def import_openlyrics(request: HttpRequest):
    """
    This function imports a list of OpenLyrics files to Tehila.
    """
    response = {
        'input_files': len(request.FILES),
        'songs_added': 0,
        'sics_created': 0,
        'sics_deleted': 0,
        'lyrics_created': 0,
    }
    known_collections = set(Collection.objects.values_list('title', flat=True))
    xml_files = []
    for file_number in request.FILES:
        f = request.FILES[file_number]
        if f.content_type == 'text/xml':
            xml_files.append([f.name, f.file.getvalue().decode('utf-8')])
        elif f.content_type == 'application/x-zip-compressed':
            with zipfile.ZipFile(f.file, mode='r') as archive:
                for filename in archive.namelist():
                    xml_files.append([filename, archive.read(filename).decode('utf-8')])
        else:
            raise ValidationError(f'File {f.name} does not have content-type text/xml or application/x-zip-compressed')
    try:
        with transaction.atomic():
            for xml in xml_files:
                song_created, sics_created, sic_deleted_count, lyrics_created = process_openlyrics(xml[1], xml[0], known_collections)
                if song_created:
                    response['songs_added'] += 1
                response['sics_created'] += sics_created
                response['sics_deleted'] += sic_deleted_count
                if lyrics_created:
                    response['lyrics_created'] += 1
    except ValidationError as ex:
        return JsonResponse({'error': ex.messages}, status=400)
    return JsonResponse(response)


@user_passes_test(lambda u: u.is_superuser)
def get_stats(_: HttpRequest):
    collection_data = Collection.objects.annotate(
        num_songs=Count("songs")
    ).annotate(
        no_year=Count("songs", filter=Q(songs__release_year=None))
    ).annotate(
        no_number=Count("songs", filter=Q(song_set__number=None))
    )
    response = {
        "collection_data": sorted(list(map(lambda c: {
            "title": c.title,
            "num_songs": c.num_songs,
            "no_release_year": c.no_year,
            "no_number": c.no_number
        }, collection_data)), key=itemgetter('num_songs'), reverse=True),
        "total_songs": Song.objects.count(),
        "non_collection_songs": Song.objects.filter(uuid=None).count()
    }
    return JsonResponse(response)

@user_passes_test(lambda u: u.is_superuser)
def parse_musicxml(request: HttpRequest):
    '''
    Parses a MusicXML file to the OpenLyrics with directions model.

    This function is only exposed to superusers because the XML parser that is used
    is vulnerable for some exploits.
    '''
    musicxml_file = request.FILES['0']
    with zipfile.ZipFile(musicxml_file.file, mode='r') as archive:
        for filename in archive.namelist():
            if filename == 'score.xml':
                raw_xml = archive.read(filename).decode('utf-8')
                openlyrics = musicxml_to_openlyrics(raw_xml)
                if 'band' not in request.GET:
                    return JsonResponse({'data': openlyrics})
                saved_object = OpenLyrics.objects.create(
                    band=Band.objects.get(pk=request.GET['band']),
                    song=Song.objects.get(pk=request.GET['song']),
                    name='MusicXML import',
                    content=openlyrics
                )
                return JsonResponse({'id': saved_object.pk})
    return HttpResponse("No score.xml found", status=400)