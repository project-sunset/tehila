from ctypes import CDLL, c_char_p
from os.path import dirname

shared_library = CDLL(f'{dirname(__file__)}/match_score.so')


def match_score(search_term: str, title: str) -> int:
    return shared_library.match_score(c_char_p(search_term.encode('utf-8')), c_char_p(title.encode('utf-8')))
