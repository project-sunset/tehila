from django.urls import include, path
from rest_framework import routers

from .views import (CollectionViewSet, LyricsViewSet, OpenLyricsViewSet,
                    SongInCollectionViewSet, SongViewSet, TagViewSet, UserOpenLyricsSettingsViewSet,
                    import_openlyrics, get_stats, parse_musicxml, PdfMusicViewSet)

router = routers.SimpleRouter()
router.register('songs', SongViewSet)
router.register('collections', CollectionViewSet)
router.register('song-in-collections', SongInCollectionViewSet)
router.register('tags', TagViewSet)
router.register('open-lyrics', OpenLyricsViewSet, basename='open-lyrics')
router.register('open-lyrics-settings', UserOpenLyricsSettingsViewSet, basename='open-lyrics-settings')
router.register('lyrics', LyricsViewSet)
router.register('pdf-music', PdfMusicViewSet, basename='pdf-music')

urlpatterns = [
    path('api/', include(router.urls)),
    path('api/import-openlyrics/', import_openlyrics, name='import-openlyrics'),
    path('api/songs-data/', get_stats, name='get-stats'),
    path('api/parse-musicxml/', parse_musicxml, name='parse-musicxml')
]
