
from tehila.songs.glue import match_score
from tehila.songs.models import Collection, Song
from tehila.songs.serializers import SongSerializer
import re

COLLECTION_NUMBER = re.compile(r'\d+[a-zA-Z]?')


def find_matching_songs(search_term: str):
    data = find_songs_matching_collection_number(search_term)
    seen = set([s['song'].pk for s in data])
    data.extend(find_songs_matching_title(search_term, seen))
    serializer = SongSerializer(data=[x['song'] for x in data[:10]], many=True)
    serializer.is_valid()
    return data, serializer

def find_songs_matching_title(search_term: str, seen: set):
    results = []
    for song in Song.objects.all():
        score = match_score(search_term, song.title)
        if score >= 0 and song.pk not in seen:
            results.append({
                'score': score,
                'song': song
            })
    results.sort(key=lambda x: x['score'], reverse=True)
    return results

def find_songs_matching_collection_number(search_term: str):
    match = COLLECTION_NUMBER.search(search_term)
    if match is None:
        return []
    collection_number = match.group(0)
    matching_songs = [
        {
            'song': s, 
            'collection_titles': set(s.song_in_collection.filter(number__iexact=collection_number).values_list('collection__title', flat=True))
        } for s in Song.objects.filter(song_in_collection__number__iexact=collection_number).distinct()
    ]
    if match.start(0) != 0:
        possible_collection_name = search_term[:match.start(0)-1]
        collection_matches = Collection.objects.filter(title__icontains=possible_collection_name)
        if collection_matches.exists():
            matching_collection_names = set(collection_matches.values_list('title', flat=True))
            return [s for s in matching_songs if len(s['collection_titles'] & matching_collection_names) > 0 not in matching_collection_names]
    return matching_songs