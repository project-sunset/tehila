from collections import Counter
import xml.etree.ElementTree as ET
from datetime import datetime
from typing import TypedDict


class MusicXmlSupportException(Exception):
    pass

part_name_transform = {
    'verse': 'vers ',
    'chorus': 'refrein ',
    'bridge': 'bridge ',
    'tag': 'tag '
}

def musicxml_to_openlyrics(musicxml_str: str):
    '''
    This function is rather janky. Probably in a few weeks even I don't know what is happening.
    But that is okay, because I am having fun now. And since this is parsing XML's uploaded by
    users I probably won't expose it publically anyway.
    '''
    musicxml = ET.fromstring(musicxml_str)
    parts = musicxml.findall('part')
    if (len(parts) != 1):
        raise MusicXmlSupportException(f"Parser only supports 1 part, found: {len(parts)}")
    current_verse = {'name': 'intro', 'text': []}
    verses = [current_verse]
    segnos = {}
    measures = parts[0].findall('measure')
    current_measure = 1
    seen_measures = Counter()
    use_coda = False
    verse_name_counter = Counter()
    jump_backs = 0
    measure_to_go_back_to = None
    go_back_barline = False
    go_back_barline_seen = set()
    seek_line = {}
    while True:
        measure = measures[current_measure-1]

        # Check if we need to skip to a next numbered line
        if measure.find("barline[@location='left']/ending[@number='1'][@type='start']") is not None:
            if current_measure in seek_line:
                new_measure = musicxml.find(f".//measure/barline[@location='left']/ending[@number='{seek_line[current_measure]}'][@type='start']/../..")
                seek_line[current_measure] += 1
                current_measure = int(new_measure.attrib['number'])
                measure = measures[current_measure - 1]
            else:
                seek_line[current_measure] = 2

        # Check if we need to start a new verse
        new_verse_name_ele = measure.find('direction/direction-type/rehearsal')
        if new_verse_name_ele is not None:
            new_verse_name = part_name_transform[new_verse_name_ele.text.strip().split(' ')[0].lower()]
            verse_name_counter[new_verse_name] += 1
            current_verse = {'name': new_verse_name + str(verse_name_counter[new_verse_name]), 'text': []}
            verses.append(current_verse)

        # Check if Segno is here
        segno_ele = measure.find('direction/sound[@segno]')
        if segno_ele is not None:
            segnos[segno_ele.attrib['segno']] = int(measure.attrib['number'])

        # Process measure
        current_chord = {'chord': None, 'text': '', 'newbar': True}
        current_verse['text'].append(current_chord)
        for element in measure:
            if element.tag == 'barline':
                if element.attrib['location'] == 'left' and element.find("repeat[@direction='forward']") is not None:
                    measure_to_go_back_to = current_measure - 1
                if element.attrib['location'] == 'right' and element.find("repeat[@direction='backward']") is not None:
                    if current_measure not in go_back_barline_seen:
                        go_back_barline = True
                        go_back_barline_seen.add(current_measure)
            elif element.tag == 'harmony':
                root = element.find('root/root-step').text
                root_alter_ele = element.find('root/root-alter')
                if root_alter_ele is not None:
                    root_alter = 'b' if root_alter_ele.text == '-1' else '#'
                else:
                    root_alter = ''
                kind = element.find('kind').attrib['text'] if 'text' in element.find('kind').attrib else ''
                bass = ''
                bass_ele = element.find('bass/bass-step')
                if bass_ele is not None:
                    bass = '/' + bass_ele.text
                    bass_alter_ele = element.find('bass/bass-alter')
                    if bass_alter_ele is not None:
                        bass += 'b' if bass_alter_ele.text == '-1' else '#'
                if current_chord['text'] == '' and current_chord['chord'] is None:
                    current_chord['chord'] = root + root_alter + kind + bass
                else:
                    current_chord = {'chord': root + root_alter + kind + bass, 'text': '', 'newbar': False}
                    current_verse['text'].append(current_chord)
            elif element.tag == 'note':
                seen_count = 0 if current_measure not in seen_measures else seen_measures[current_measure]
                available_lyric_numbers = [int(l.attrib['number']) for l in element.findall('lyric')]
                if len(available_lyric_numbers) != 0:
                    selected_number = max([number for number in available_lyric_numbers if number <= seen_count+1])
                    lyric = element.find(f"lyric[@number='{selected_number}']")
                    if lyric is not None:
                        syllabic = lyric.find('syllabic').text
                        word = lyric.find("text").text.strip()
                        if syllabic == 'single':
                            current_chord['text'] += f'{word} '
                        elif syllabic == 'begin':
                            current_chord['text'] += word
                        elif syllabic == 'middle':
                            current_chord['text'] += word
                        elif syllabic == 'end':
                            current_chord['text'] += f'{word} '
        
        seen_measures[current_measure] += 1

        # Check if dalsegno is here
        dalsegno_ele = measure.find('direction/sound[@dalsegno]')
        if dalsegno_ele is not None:
            jump_backs += 1
            current_measure = segnos[dalsegno_ele.attrib['dalsegno']]
            del segnos[dalsegno_ele.attrib['dalsegno']]
            if 'al Coda' in measure.find('direction/direction-type/words').text:
                use_coda = True
            continue
        
        # Check if To Coda is here
        tocoda_ele = measure.find('direction/sound[@tocoda]')
        if tocoda_ele is not None and use_coda:
            coda_measure = parts[0].find(f"measure/direction/sound[@coda='{tocoda_ele.attrib['tocoda']}']/../..")
            coda_measure_number = int(coda_measure.attrib['number'])
            if coda_measure_number <= current_measure:
                raise MusicXmlSupportException("Coda is behind To Coda?!")
            current_measure = coda_measure_number
            use_coda = False
            continue

        if go_back_barline:
            go_back_barline = False    
            current_measure = measure_to_go_back_to
            jump_backs += 1

        if jump_backs > 10:
            raise MusicXmlSupportException("More than 10 jump backs")
        if len(measures) == current_measure:
            break
        current_measure += 1
    return create_openlyrics(musicxml.find('.//work-title').text, verses)

class MusicXmlChord(TypedDict):
    chord: str
    text: str
    newbar: bool

class MusicXmlVerse(TypedDict):
    name: str
    text: list[MusicXmlChord]

def create_openlyrics(title: str, song: list[MusicXmlVerse]):
    root = ET.Element('song')
    root.set('xmlns', 'http://openlyrics.info/namespace/2009/song')
    root.set('version', '0.8')
    root.set('createdIn', 'BobsPython')
    root.set('modifiedIn', 'BobsPython')
    root.set('modifiedDate', datetime.now().isoformat(timespec='seconds'))

    properties = ET.SubElement(root, 'properties')
    titles = ET.SubElement(properties, 'titles')
    t = ET.SubElement(titles, 'title')
    t.text = title

    directions = ET.SubElement(root, 'directions')
    lyrics = ET.SubElement(root, 'lyrics')
    for verse in song:
        section = ET.SubElement(directions, 'section')
        section.set('verse', verse['name'])
        v = ET.SubElement(lyrics, 'verse')
        v.set('name', verse['name'])

        lines = ET.SubElement(v, 'lines')

        for chord in verse['text']:
            c = ET.SubElement(lines, 'chord')
            if chord['chord'] is not None:
                c.set('root', chord['chord'])
            if chord['newbar']:
                c.set('newbar', 'true')
            c.text = chord['text']

    return ET.tostring(root, encoding='unicode')