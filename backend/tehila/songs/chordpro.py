import xml.etree.ElementTree as ET

NS = '{http://openlyrics.info/namespace/2009/song}'

def chord_name(chord: ET.Element):
    result = chord.attrib['root']
    if 'structure' in chord.attrib:
        result += chord.attrib['structure']
    if 'bass' in chord.attrib:
        result += f"/{chord.attrib['bass']}"
    return result

def openlyrics_to_chordpro(title: str, openlyrics_xml_str: str) -> str:
    chordpro = f'{{title: {title}}}\n\n'
    root = ET.fromstring(openlyrics_xml_str)
    verse_order = [ele.attrib['verse'] for ele in  root.findall(f'.//{NS}section')]
    for verse_name in verse_order:
        verse = root.find(f".//{NS}verse[@name='{verse_name}']")
        chordpro += f'{{start_of_verse: {verse_name}}}\n'
        for lines in verse:
            if lines.text is not None:
                chordpro += lines.text
            for chord_ele in lines:
                if chord_ele.tag == f'{NS}br':
                    chordpro += '\n'
                else:
                    chordpro += f'[{chord_name(chord_ele)}]{chord_ele.text}'
                    if chord_ele.tail is not None:
                        chordpro += chord_ele.tail
        chordpro += '\n'
        chordpro += '{end_of_verse}\n\n'
    return chordpro