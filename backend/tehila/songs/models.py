from uuid import uuid4

from django.conf import settings
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.db.models import Q


class Tag(models.Model):
    name = models.CharField(max_length=40)
    background_color = models.CharField(max_length=40)
    text_color = models.CharField(max_length=40)

    def __str__(self):
        return self.name


class Song(models.Model):
    uuid = models.UUIDField(null=True, blank=True, unique=True)  # used if it is part of a collection
    source = models.CharField(max_length=100, null=True, blank=True)  # used if it is not part of a collection
    title = models.CharField(max_length=100)
    release_year = models.IntegerField(null=True, blank=True, validators=[
        MinValueValidator(1000),
        MaxValueValidator(2050)
    ])
    tags = models.ManyToManyField(Tag, related_name="songs", blank=True)
    text = models.TextField(null=True, blank=True)  # only used for searching

    class Meta:
        constraints = [
            models.CheckConstraint(check=Q(uuid__isnull=False, source=None) | Q(source__isnull=False, uuid=None), name='')
        ]

    def __str__(self):
        return self.title


class Lyrics(models.Model):
    song = models.OneToOneField(Song, on_delete=models.CASCADE)
    plain = models.TextField()
    xml = models.TextField()

    def __str__(self):
        return f'Lyrics of {self.song}'

class PdfMusic(models.Model):
    uuid = models.UUIDField(default=uuid4, editable=False, unique=True)
    band = models.ForeignKey('Band', on_delete=models.CASCADE, blank=False, null=True)
    song = models.ForeignKey(Song, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)

    def __str__(self):
        return f'{self.band} - {self.song} - {self.name}'
    
def get_image_filename(instance, filename):
    band_uuid = instance.pdfmusic.band.uuid
    music_uuid = instance.pdfmusic.uuid
    page_number = instance.page_number
    return f'pdf-pages/{band_uuid}/{music_uuid}/{page_number}.png'

class PdfPage(models.Model):
    pdfmusic = models.ForeignKey(PdfMusic, on_delete=models.CASCADE, related_name='pages')
    page_number = models.IntegerField()
    image = models.FileField(upload_to=get_image_filename, storage=settings.B2_STORAGE, null=True)

    def __str__(self):
        return f'{self.pdfmusic} - {self.page_number}'

class OpenLyrics(models.Model):
    band = models.ForeignKey('Band', on_delete=models.CASCADE, blank=False, null=True)
    song = models.ForeignKey(Song, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    content = models.TextField()

    def __str__(self):
        return f'{self.band} - {self.song} - {self.name}'


class UserOpenLyricsSettings(models.Model):
    openlyrics = models.ForeignKey(OpenLyrics, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    use_flats = models.BooleanField(null=True, blank=True)
    hide_repeats = models.BooleanField(null=True, blank=True)
    show_chords = models.BooleanField(null=True, blank=True)
    transpose = models.IntegerField(null=True, blank=True,
                                    validators=[
                                        MaxValueValidator(11),
                                        MinValueValidator(-11)
                                    ])

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['openlyrics', 'user'], name='unique_settings')
        ]

    def __str__(self):
        return f"{self.user}'s settings - {self.openlyrics}"


class Collection(models.Model):
    title = models.CharField(max_length=100, unique=True)
    songs = models.ManyToManyField(Song, through="SongInCollection", blank=True)

    def __str__(self):
        return self.title


class SongInCollection(models.Model):
    song = models.ForeignKey(Song, on_delete=models.CASCADE, related_name='song_in_collection')
    collection = models.ForeignKey(Collection, on_delete=models.CASCADE, related_name='song_set')
    number = models.CharField(max_length=10, null=True, blank=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['song', 'collection'], name='unique_songincollection')
        ]

    def __str__(self):
        return f"{self.collection} - {self.number} {self.song}"
