from rest_framework import serializers

from .models import Collection, Lyrics, OpenLyrics, PdfMusic, PdfPage, Song, SongInCollection, Tag, UserOpenLyricsSettings


class UserOpenLyricsSettingsSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserOpenLyricsSettings
        fields = ('id', 'openlyrics', 'transpose', 'use_flats', 'hide_repeats', 'show_chords')

SHEET_MUSIC_FIELDS = ('id', 'band', 'song', 'name')

class OpenLyricsNoContentSerializer(serializers.ModelSerializer):
    band = serializers.CharField(source='band.name')

    class Meta:
        model = OpenLyrics
        fields = SHEET_MUSIC_FIELDS


class OpenLyricsSerializer(serializers.ModelSerializer):
    class Meta:
        model = OpenLyrics
        fields = ('id', 'band', 'song', 'name', 'content')

class PdfMusicNoNestedSerializer(serializers.ModelSerializer):
    band = serializers.CharField(source='band.name')
    
    class Meta:
        model = PdfMusic
        fields = SHEET_MUSIC_FIELDS

class PdfPageSerializer(serializers.ModelSerializer):
    class Meta:
        model = PdfPage
        fields = ('id', 'page_number', 'image')

class PdfMusicSerializer(serializers.ModelSerializer):
    pages = PdfPageSerializer(many=True)

    class Meta:
        model = PdfMusic
        fields = ('id', 'band', 'song', 'name', 'pages')


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ('id', 'name', 'background_color', 'text_color')


class SongTagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ('id', 'name', 'background_color', 'text_color')
        extra_kwargs = {'name': {'required': False}, 'background_color': {'required': False}, 'text_color': {'required': False}}


class CollectionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Collection
        fields = ('id', 'title')


class SongInCollectionSerializer(serializers.ModelSerializer):
    collection = CollectionSerializer(read_only=True)

    class Meta:
        model = SongInCollection
        fields = ('collection', 'number')


class SimpleSongSerializer(serializers.ModelSerializer):
    class Meta:
        model = Song
        fields = ('id', 'title')


class SongSerializer(serializers.ModelSerializer):
    collections = SongInCollectionSerializer(many=True, required=False, source='song_in_collection')
    tags = SongTagSerializer(many=True, required=False)

    class Meta:
        model = Song
        fields = ('id', 'title', 'release_year', 'collections', 'tags', 'source')

    def validate(self, data):
        source_missing = 'source' not in data or data['source'] is None
        if hasattr(self, 'initial_data') and 'id' in self.initial_data:
            current = Song.objects.get(pk=self.initial_data['id'])
            if current.uuid is None and source_missing:
                raise serializers.ValidationError("Artiest, bundel, collectie is verplicht")
        elif hasattr(self, 'parent') and hasattr(self.parent, 'initial_data') and 'id' in self.parent.initial_data['song']:
            current = Song.objects.get(pk=self.parent.initial_data['song']['id'])
            if current.uuid is None and source_missing:
                raise serializers.ValidationError("Artiest, bundel, collectie is verplicht")
        else:
            if source_missing:
                raise serializers.ValidationError("Artiest, bundel, collectie is verplicht")
        return data

    def create(self, validated_data):
        # remove relations from validated data
        collections = validated_data.pop('song_in_collection', None)
        tags = validated_data.pop('tags', None)
        # create new item
        song = Song.objects.create(**validated_data)
        # create through model instances
        if collections:
            for song_in_collection in self.get_initial()['collections']:
                collection_id = song_in_collection['collection']['id']
                SongInCollection.objects.create(song=song, collection_id=collection_id, number=song_in_collection['number'])
        if tags:
            for tag in self.get_initial()['tags']:
                song.tags.add(Tag.objects.get(pk=tag['id']))
        return song

    def update(self, instance: Song, validated_data):
        # remove relations from validated data
        collections = validated_data.pop('song_in_collection', None)
        tags = validated_data.pop('tags', None)
        # update item
        for attr, value in validated_data.items():
            setattr(instance, attr, value)
        instance.save()
        relations_to_remove = list(instance.song_in_collection.all())
        # update or create relations
        if collections:
            for song_in_collection in self.get_initial()['collections']:
                collection_pk = song_in_collection['collection']['id']
                obj, created = SongInCollection.objects.update_or_create(
                    song=instance, collection_id=collection_pk, defaults={
                        'number': song_in_collection['number']})
                if not created:
                    relations_to_remove = [
                        i for i in relations_to_remove if i != obj]
        for m in relations_to_remove:
            m.delete()
        tags_to_remove = list(instance.tags.all())
        if tags:
            for tag in self.get_initial()['tags']:
                current_tag = Tag.objects.get(pk=tag['id'])
                instance.tags.add(current_tag)
                tags_to_remove = [i for i in tags_to_remove if i != current_tag]
        for tag in tags_to_remove:
            instance.tags.remove(tag)
        return instance

class LyricsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Lyrics
        fields = ('id', 'xml')