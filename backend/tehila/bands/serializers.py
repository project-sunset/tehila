from PIL import Image
from rest_framework import serializers
from tehila.recordings.serializers import RecordingSerializer
from io import BytesIO
from .models import Band, BandMember, BandSong, Church, Invite


class BandFilteredListSerializer(serializers.ListSerializer):
    def to_representation(self, data):
        data = data.filter(band=self.context['request'].tehila_band)
        return super().to_representation(data)


class BandSongSerializer(serializers.ModelSerializer):
    recordings = RecordingSerializer(many=True, read_only=True)

    class Meta:
        list_serializer_class = BandFilteredListSerializer
        model = BandSong
        fields = ('id', 'recordings', 'song', 'public_recording')


class BandSerializer(serializers.ModelSerializer):
    class Meta:
        model = Band
        fields = ('id', 'name', 'uuid', 'background_picture', 'church')


class BandUserSerializer(serializers.ModelSerializer):
    name = serializers.CharField(source="user.username")
    email = serializers.CharField(source="user.email")

    class Meta:
        model = BandMember
        fields = ('id', 'name', 'email')


class InviteSerializer(serializers.ModelSerializer):
    band = BandSerializer()
    sender = BandUserSerializer()
    invitee = BandUserSerializer()

    class Meta:
        model = Invite
        fields = ('id', 'band', 'sender', 'invitee', 'created_time')


class BandWithMembersSerializer(serializers.ModelSerializer):
    bandmembers = BandUserSerializer(read_only=True, many=True)
    send_invites = InviteSerializer(read_only=True, many=True)

    class Meta:
        model = Band
        fields = ('id', 'name', 'uuid', 'bandmembers', 'send_invites', 'background_picture', 'church')

    def validate_background_picture(self, value):
        if value.image.format != 'JPEG':
            raise serializers.ValidationError('Image has to be a jpeg')
        if value.size > 200 * 1024 * 1024:
            raise serializers.ValidationError('Image has to be smaller than 200kb')
        if value.image.height != value.image.width:
            new_dimension = min(value.image.height, value.image.width)
            top = 0 + round((value.image.height - new_dimension)/2)
            right = 0 + round((value.image.width - new_dimension)/2)
            image = Image.open(value.file)
            cropped_image = image.crop((right, top, right+new_dimension, top+new_dimension))
            new_file = BytesIO()
            cropped_image.save(new_file, format='JPEG')
            value.file = new_file
            value.image = cropped_image
        return value


class BandMemberSerializer(serializers.ModelSerializer):
    bands = BandSerializer(read_only=True, many=True)

    class Meta:
        model = BandMember
        fields = ('id', 'bands')


class ChurchSerializer(serializers.ModelSerializer):
    class Meta:
        model = Church
        fields = ('id', 'name', 'logo', 'background_picture')
