from uuid import uuid4

from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from tehila.songs.models import Song


def get_logo_upload_name_church(instance, filename):
    extension = filename[filename.rfind('.'):]
    return f'pictures/church-logo-{instance.id}{extension}'


def get_bg_upload_name_church(instance, filename):
    extension = filename[filename.rfind('.'):]
    return f'pictures/church-{instance.id}{extension}'


def get_bg_upload_name_band(instance, filename):
    extension = filename[filename.rfind('.'):]
    return f'pictures/band-{instance.id}{extension}'


class Church(models.Model):
    name = models.CharField(max_length=100, unique=True)
    logo = models.ImageField(upload_to=get_logo_upload_name_church, storage=settings.B2_STORAGE, null=True, blank=True)
    background_picture = models.ImageField(upload_to=get_bg_upload_name_church, storage=settings.B2_STORAGE, null=True, blank=True)

    def __str__(self):
        return self.name


class Band(models.Model):
    church = models.ForeignKey(Church, on_delete=models.SET_NULL, blank=True, null=True, related_name='bands')
    name = models.CharField(max_length=100)
    songs = models.ManyToManyField(Song, through='BandSong')
    uuid = models.UUIDField(default=uuid4, editable=False, unique=True)
    background_picture = models.ImageField(upload_to=get_bg_upload_name_band, storage=settings.B2_STORAGE, null=True, blank=True)

    def __str__(self):
        return self.name


class BandMember(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, unique=True)
    bands = models.ManyToManyField(Band, blank=True, related_name='bandmembers')

    def __str__(self):
        return f"band member {self.user}"


class Invite(models.Model):
    band = models.ForeignKey(Band, on_delete=models.CASCADE, related_name='send_invites')
    sender = models.ForeignKey(BandMember, on_delete=models.CASCADE, related_name='send_invites')
    invitee = models.ForeignKey(BandMember, on_delete=models.CASCADE, related_name='received_invites')
    created_time = models.DateTimeField(auto_now_add=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['band', 'invitee'], name='unique_invite')
        ]

    def __str__(self):
        return f'invite from {self.band} to {self.invitee}'


class BandSong(models.Model):
    song = models.ForeignKey(Song, on_delete=models.CASCADE, related_name='bandsong')
    band = models.ForeignKey(Band, on_delete=models.CASCADE)
    public_recording = models.ForeignKey('Recording', on_delete=models.SET_NULL, blank=True, null=True, related_name='is_public_for')

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['song', 'band'], name='unique_bandsong')
        ]

    def __str__(self):
        return f'{self.song} - {self.band}'
