from django.test import tag
from django.test.client import Client
from tehila.utils_test import BaseTestCase, client


@tag('bands')
class BandsTests(BaseTestCase):

    @client('David')
    def test_multiple_bands(self, david: Client):
        response = david.get('/accounts/login/')
        self.assertEqual(response.status_code, 200, msg=response.content)
        self.assertEqual(2, len(response.json()['user']['tehila']))

    @client('Jubal')
    def test_single_bands(self, jubal: Client):
        response = jubal.get('/accounts/login/')
        self.assertEqual(response.status_code, 200, msg=response.content)
        self.assertEqual(1, len(response.json()['user']['tehila']['bands']))
        self.assertEqual("String boys", response.json()['user']['tehila']['bands'][0]['name'])

    @client('David')
    def test_todos(self, david: Client):
        response = david.get('/api/todos/', headers={'x-tehila-band': 1})
        self.assertEqual(200, response.status_code)
        content = response.json()
        self.assertEqual(1, len(content['add_lyrics']))
        self.assertEqual('Unwritten Psalm', content['add_lyrics'][0]['title'])

    @client('David')
    def test_internal_repertoire(self, david: Client):
        response = david.get('/api/repertoire/', headers={'x-tehila-band': 1})
        self.assertEqual(200, response.status_code)
        data = response.json()['data']
        self.assertEqual(2, len(data))

    def test_external_repertoire(self):
        c = Client()
        response = c.get('/api/repertoire/?bandname=Original Psalm Project')
        self.assertEqual(200, response.status_code)
        data = response.json()['data']
        self.assertEqual(2, len(data))

    @client('David')
    def test_stats(self, david: Client):
        response = david.get('/api/stats/1/')
        self.assertEqual(200, response.status_code)
