from django.contrib import admin

from .models import Band, BandMember, BandSong, Church


@admin.register(Band)
class BandAdmin(admin.ModelAdmin):
    list_display = ('name',)


@admin.register(BandSong)
class BandSongAdmin(admin.ModelAdmin):
    list_display = ('song', 'band')


@admin.register(BandMember)
class SongsUserAdmin(admin.ModelAdmin):
    list_display = ('user',)


@admin.register(Church)
class ChurchAdmin(admin.ModelAdmin):
    list_display = ('name',)
