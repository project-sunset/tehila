import csv
import io
import zipfile

from django.contrib.auth.decorators import login_required
from django.core.cache import cache
from django.core.exceptions import PermissionDenied
from django.db import connection
from django.http import HttpResponse
from django.http.request import HttpRequest
from django.http.response import JsonResponse
from django.utils.text import slugify
from rest_framework import viewsets
from rest_framework.request import Request
from rest_framework.decorators import action, api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from tehila.recordings.models import Recording
from tehila.songs.models import Song, Tag
from tehila.songs.serializers import SimpleSongSerializer

from backend.permissions import HasBandSelected, IsInvitee, IsOwner, IsSuperuserOrReadOnly

from .models import Band, BandMember, BandSong, Church, Invite
from .serializers import (BandMemberSerializer, BandSerializer,
                          BandSongSerializer, BandWithMembersSerializer,
                          ChurchSerializer, InviteSerializer)


class InviteViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, IsInvitee)
    serializer_class = InviteSerializer
    pagination_class = None

    def get_queryset(self):
        return Invite.objects.filter(invitee__user=self.request.user)

    @action(methods=['post'], detail=True)
    def accept(self, _, pk):
        invite = Invite.objects.get(pk=pk)
        band = invite.band
        bandmember = BandMember.objects.get(user=self.request.user)
        bandmember.bands.add(band)
        bandmember.save()
        invite.delete()
        return HttpResponse(status=204)


class ChurchViewSet(viewsets.ModelViewSet):
    permission_classes = (IsSuperuserOrReadOnly,)
    serializer_class = ChurchSerializer
    pagination_class = None
    queryset = Church.objects.all()


class BandViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = BandSerializer
    pagination_class = None
    filter_fields = ('name',)

    def get_queryset(self):
        return Band.objects.filter(bandmembers__user=self.request.user)

    def get_serializer_class(self):
        if self.action == 'list':
            return BandSerializer
        return BandWithMembersSerializer


class BandSongViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,HasBandSelected,)
    serializer_class = BandSongSerializer
    pagination_class = None
    filter_fields = ('song',)

    def get_queryset(self):
        return BandSong.objects.filter(band=self.request.tehila_band)

    def perform_create(self, serializer):
        serializer.save(band=self.request.tehila_band)


class BandMemberViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, IsOwner)
    serializer_class = BandMemberSerializer
    pagination_class = None

    def get_queryset(self):
        return BandMember.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    @action(methods=['put'], detail=False)
    def remove(self, request):
        band = Band.objects.get(pk=request.data['band'])
        caller = BandMember.objects.get(user_id=request.user.id)
        if not band.bandmembers.filter(id=caller.id).exists():
            return PermissionDenied
        bandmember = BandMember.objects.get(pk=request.data['bandmember'])
        bandmember.bands.remove(band)
        return HttpResponse(status=204)

    @action(methods=['put'], detail=False)
    def invite(self, request):
        band = Band.objects.get(pk=request.data['band'])
        sender = BandMember.objects.get(user_id=request.user.id)
        if not band.bandmembers.filter(id=sender.id).exists():
            return PermissionDenied
        try:
            invitee = BandMember.objects.get(user__username=request.data['invitee'])
        except BandMember.DoesNotExist:
            return HttpResponse(status=400, content=f"{request.data['invitee']} bestaat niet.")
        if band.bandmembers.filter(id=invitee.id).exists():
            return HttpResponse(status=400, content=f"{request.data['invitee']} is al lid.")
        invite = Invite(band=band, sender=sender, invitee=invitee)
        invite.save()
        return JsonResponse(InviteSerializer(invite).data)

    @invite.mapping.delete
    def delete_invite(self, request):
        band = Band.objects.get(pk=request.data['band'])
        sender = BandMember.objects.get(user_id=request.user.id)
        if not band.bandmembers.filter(id=sender.id).exists():
            return PermissionDenied
        invite = Invite(band=band, pk=request.data['id'])
        invite.delete()
        return HttpResponse(status=204)

@api_view(['get'])
@permission_classes([HasBandSelected])
def todos(request: Request):
    if request.user.is_superuser:
        songs_without_lyrics = Song.objects.filter(eventsong__event__band=request.tehila_band, text=None).distinct()
    else:
        songs_without_lyrics = []
    songs_without_tags = Song.objects.filter(eventsong__event__band=request.tehila_band, tags=None).distinct()
    songs_without_release_year = Song.objects.filter(eventsong__event__band=request.tehila_band, release_year=None).distinct()
    songs_without_collection = Song.objects.filter(eventsong__event__band=request.tehila_band, song_in_collection=None).distinct()
    return JsonResponse({
        'add_lyrics': SimpleSongSerializer(songs_without_lyrics, many=True).data,
        'add_tags': SimpleSongSerializer(songs_without_tags, many=True).data,
        'add_release_year': SimpleSongSerializer(songs_without_release_year, many=True).data,
        'add_collection': SimpleSongSerializer(songs_without_collection, many=True).data
    })


def repertoire(request: HttpRequest):
    if 'bandname' in request.GET:
        band = Band.objects.get(name__iexact=request.GET.get('bandname'))
    elif hasattr(request, 'tehila_band'):
        band = request.tehila_band
    else:
        return HttpResponse(status=400)
    return JsonResponse({'data': get_repertoire(band.pk)})


def get_repertoire(band: int):
    cached_data = cache.get(f'{band}-repertoire')
    if cached_data is not None:
        return cached_data
    with connection.cursor() as cursor:
        cursor.execute(
            '''
            SELECT 
                a.id, 
                title, 
                COUNT(c.id) AS count, 
                MAX(c.date) AS last_played, 
                r.audio,
                r.id AS recording_id,
                GROUP_CONCAT(DISTINCT st.tag_id) as tags
            FROM
                tehila_song a
                    INNER JOIN tehila_eventsong b
                        ON a.id = b.song_id
                    INNER JOIN tehila_event c
                        ON c.id = b.event_id
                    LEFT JOIN tehila_bandsong bs
                        ON a.id = bs.song_id
                    LEFT JOIN tehila_recording r
                        ON bs.public_recording_id = r.id
                    LEFT JOIN tehila_song_tags st
                        ON st.song_id = a.id
            WHERE 
                c.band_id = %s
                AND c.event_type = 'SERVICE'
            GROUP BY a.id
            ''',
            [band]
        )
        raw_data = cursor.fetchall()
    data = []
    tags = {str(t.id): t for t in Tag.objects.all()}
    for row in raw_data:
        data.append({
            'id': row[0],
            'title': row[1],
            'count': row[2],
            'last_played': row[3],
            'audio_url': Recording.audio.field.storage.url(row[4]) if row[4] is not None else None,
            'recording_id': row[4],
            'collections': [
                {'collection': c.collection.title, 'number': c.number}
                for c in Song.objects.get(pk=row[0]).song_in_collection.all()
            ]
        })
        if row[6] is not None:
            data[-1]['tags'] = [{'name': tags[t].name, 'background_color': tags[t].background_color, 'text_color': tags[t].text_color} for t in row[6].split(",")]
        else:
            data[-1]['tags'] = []
    cache.set(f'{band}-repertoire', data, 7*24*60*60)
    return data


@login_required
def get_stats(_: HttpRequest, band: int):
    response = {}
    repertoire_list = get_repertoire(band)
    response['total_songs'] = len(repertoire_list)
    with connection.cursor() as cursor:
        cursor.execute('''
        SELECT round(avg(song_count),1) as avg_song_count
        FROM (
            SELECT count(*) as song_count
            FROM tehila_song a
                INNER JOIN tehila_eventsong b
                    ON a.id = b.song_id
                INNER JOIN tehila_event c
                    ON c.id = b.event_id
            WHERE 
                c.band_id = %s
                AND c.event_type = 'SERVICE'
            GROUP BY event_id)
        ''', [band])
        response['avg_song_count'] = cursor.fetchall()[0][0]
        cursor.execute('''
        SELECT substr(release_year,0, 4) as decade, count(*) as count
        FROM tehila_song a
            INNER JOIN tehila_eventsong b
                ON a.id = b.song_id
            INNER JOIN tehila_event c
                ON c.id = b.event_id
        WHERE 
            c.band_id = %s
            AND c.event_type = 'SERVICE'
        GROUP BY substr(release_year,0, 4)
        ORDER BY decade desc
        ''', [band])
        songs_played_by_decade = cursor.fetchall()
        response['songs_played_by_decade'] = []
        for row in songs_played_by_decade:
            response['songs_played_by_decade'].append({
                'decade': row[0],
                'count': row[1]
            })
        cursor.execute('''
        SELECT count(*)
        FROM tehila_song a
            INNER JOIN tehila_eventsong b
                ON a.id = b.song_id
            INNER JOIN tehila_event c
                ON c.id = b.event_id
        WHERE 
            c.band_id = %s
            AND c.event_type = 'SERVICE'
        ''', [band])
        response['total_songs_played'] = cursor.fetchall()[0][0]
        cursor.execute('''
        SELECT e.title, count(*) as count
        FROM tehila_song a
            INNER JOIN tehila_eventsong b
                ON a.id = b.song_id
            INNER JOIN tehila_event c
                ON c.id = b.event_id
            INNER JOIN tehila_songincollection d
                ON d.song_id = a.id
            INNER JOIN tehila_collection e
                ON d.collection_id = e.id
        WHERE 
            c.band_id = %s
            AND c.event_type = 'SERVICE'
        GROUP BY e.title
        ORDER BY count desc
        ''', [band])
        songs_played_by_collection = cursor.fetchall()
        response['songs_played_by_collection'] = []
        for row in songs_played_by_collection:
            response['songs_played_by_collection'].append({
                'collection': row[0],
                'percentage': round((row[1]/response['total_songs_played'])*100)
            })
        cursor.execute('''
        SELECT times_played, count(*) as songs
        FROM (
            SELECT a.title, count(*) as times_played
            FROM tehila_song a
                INNER JOIN tehila_eventsong b
                    ON a.id = b.song_id
                INNER JOIN tehila_event c
                    ON c.id = b.event_id
            WHERE 
                c.band_id = %s
                AND c.event_type = 'SERVICE'
            GROUP BY a.id)
        GROUP BY times_played
        ORDER BY times_played
        ''', [band])
        repetition_count = cursor.fetchall()
        response['repetition_count'] = []
        for row in repetition_count:
            response['repetition_count'].append({
                'times_played': row[0],
                'songs': row[1]
            })
    return JsonResponse(response)


@login_required
def export(request: HttpRequest, band_id: int):
    try:
        band = Band.objects.get(pk=band_id, bandmembers__user=request.user)
    except Band.DoesNotExist:
        return HttpResponse(status=400)
    repertoire = get_repertoire(band_id)
    data = io.BytesIO()
    with zipfile.ZipFile(data, mode='w') as zip:
        repertoire_data = io.StringIO()
        writer = csv.DictWriter(repertoire_data, fieldnames=['Title', 'Last played', 'Times played', 'Tags', 'Collections'])
        writer.writeheader()
        for entry in repertoire:
            writer.writerow({
                'Title': entry['title'],
                'Last played': entry['last_played'],
                'Times played': entry['count'],
                'Tags': ', '.join([tag['name'] for tag in entry['tags']]),
                'Collections': ', '.join([f"{collection['collection']} {collection['number']}" for collection in entry['collections']])
            })
            if entry['recording_id'] is not None:
                zip.writestr(slugify(f'{entry['id']}-{entry["title"]}.mp3'), Recording.audio.field.storage.open(entry['recording_id']).read())
        zip.writestr('repertoire.csv', repertoire_data.getvalue())
    response = HttpResponse(data.getvalue(), content_type='application/zip')
    response['Content-Disposition'] = f'attachment; filename={band.name}_export.zip'
    return response