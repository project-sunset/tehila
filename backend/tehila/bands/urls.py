from django.urls import include, path
from rest_framework import routers

from .views import (BandMemberViewSet, BandSongViewSet, BandViewSet,
                    ChurchViewSet, InviteViewSet, export, get_stats,
                    repertoire, todos)

router = routers.SimpleRouter()
router.register('bands', BandViewSet, basename='band')
router.register('bandsongs', BandSongViewSet, basename='bandsong')
router.register('bandmembers', BandMemberViewSet, basename="bandmember")
router.register('church', ChurchViewSet, basename='church')
router.register('invites', InviteViewSet, basename='invite')

urlpatterns = [
    path('api/', include(router.urls)),
    path('api/repertoire/', repertoire, name='repertoire'),
    path('api/todos/', todos, name='todos'),
    path('api/stats/<int:band>/', get_stats, name='stats'),
    path('api/export/<int:band_id>/', export, name='export'),
]
