import json

from django.core.exceptions import ValidationError
from django.db import models


def validate_json_array(value: str):
    try:
        parsed = json.loads(value)
        if type(parsed) is not list:
            raise ValidationError('Text is not a json array')
    except Exception:
        raise ValidationError('Text is not valid json')

class Update(models.Model):
    date = models.DateField()
    text = models.TextField(validators=[validate_json_array])

    def __str__(self):
        return str(self.date)