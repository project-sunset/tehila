from .models import Update
from django.contrib import admin

import tehila.bands.admin
import tehila.recordings.admin
import tehila.events.admin
import tehila.songs.admin
import tehila.recorder.admin




@admin.register(Update)
class UpdateAdmin(admin.ModelAdmin):
    list_display = ('date',)
