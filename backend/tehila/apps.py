from django.apps import AppConfig


class TehilaConfig(AppConfig):
    name = 'tehila'

    def ready(self):
        import tehila.signals
