from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('tehila', '0004_auto_20211212_1534'),
    ]

    operations = [
        migrations.RenameModel(old_name='Service', new_name='Event'),
        migrations.RenameModel(old_name='ServiceSong', new_name='EventSong'),
        migrations.RenameField(
            model_name='eventsong',
            old_name='service',
            new_name='event',
        ),
        migrations.AddField(
            model_name='event',
            name='event_type',
            field=models.CharField(choices=[('SERVICE', 'Service'), ('PRACTICE', 'Practice')], default='SERVICE', max_length=8),
        ),
        migrations.AlterField(
            model_name='eventsong',
            name='song',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='eventsong', to='tehila.song'),
        ),
        migrations.AddField(
            model_name='event',
            name='last_modified',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AddField(
            model_name='event',
            name='preparation_start',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='band',
            name='uuid',
            field=models.UUIDField(default=uuid.uuid4, editable=False),
        ),
    ]
