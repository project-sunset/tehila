#!/bin/bash

python3 set_relative_dates.py
rm db.sqlite3
python3 manage.py migrate 
python3 manage.py createcachetable
python3 manage.py loaddata \
  users_test_data.json \
  songs_test_data.json \
  events_test_data.json \
  bands_test_data.json \
  recorder_test_data.json \
  beamer_password.json

if [ $# -eq 0 ]
  then
    # No args supplied so this is not playwright
    python3 set_desktop_webauthn.py
fi