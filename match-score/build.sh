set -e
# Build for Python
gcc -Os -fPIC  -shared -o ../backend/tehila/songs/match_score.so match_score.c

# Build for frontend
export EMSDK_QUIET=1
source ~/emsdk/emsdk_env.sh
emcc match_score.c -Os \
  -o match_score.js \
  -s FETCH=1 \
  -s EXPORT_ES6=1 \
  -s MODULARIZE=1 \
  -s ENVIRONMENT=web \
  -s EXPORTED_FUNCTIONS=_match_score \
  -s EXPORTED_RUNTIME_METHODS=cwrap,ccall \
  --closure 1

cp match_score.js ../backend/sites/static/sites/match-score/
cp match_score.wasm ../backend/sites/static/sites/match-score/
cp match_score.js ../frontend/src/match-score/
cp match_score.wasm ../frontend/src/match-score/

rm match_score.js
rm match_score.wasm