/*
The levenshtein function is copied from https://github.com/gustf/js-levenshtein
and then rewritten to C.
*/
#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

int _min(int d0, int d1, int d2, int bx, int ay)
{
  return d0 < d1 || d2 < d1
             ? d0 > d2
                   ? d2 + 1
                   : d0 + 1
         : bx == ay
             ? d1
             : d1 + 1;
}

int levenshtein(char *a, int la, char *b, int lb)
{
  // make a always the shorter string
  if (la > lb)
  {
    char *tmp = a;
    a = b;
    b = tmp;
    int ltemp = la;
    la = lb;
    lb = ltemp;
  }

  // change length to remove common suffix
  while (la > 0 && a[la - 1] == b[lb - 1])
  {
    la--;
    lb--;
  }

  // Create start offset to remove common prefix
  int offset = 0;
  while (offset < la && a[offset] == b[offset])
  {
    offset++;
  }
  la = la - offset;
  lb = lb - offset;

  // if nothing is left of a, return the length of lb
  // if lb === 1 then la is also 1, so 1 is the answer
  // if lb === 2 then la can be 1 or 2, but 2 is always the edit distance
  if (la == 0 || lb < 3)
  {
    return lb;
  }

  int x = 0;
  int y, d0, d1, d2, d3, dd, dy, ay, bx0, bx1, bx2, bx3;

  // vector is filled with chars of shorter string
  // vector = [1, a, 2, b, 3, c]
  int vector[la * 2];
  for (y = 0; y < la; y++)
  {
    vector[y * 2] = y + 1;
    vector[(y * 2) + 1] = (int)a[offset + y];
  }

  int len = (la * 2) - 1;
  for (; x < lb - 3;)
  {
    d0 = x;
    bx0 = b[offset + d0];
    d1 = x + 1;
    bx1 = b[offset + d1];
    d2 = x + 2;
    bx2 = b[offset + d2];
    d3 = x + 3;
    bx3 = b[offset + d3];
    x = x + 4;
    dd = x;
    for (y = 0; y < len; y = y + 2)
    {
      dy = vector[y];
      ay = vector[y + 1];
      d0 = _min(dy, d0, d1, bx0, ay);
      d1 = _min(d0, d1, d2, bx1, ay);
      d2 = _min(d1, d2, d3, bx2, ay);
      dd = _min(d2, d3, dd, bx3, ay);
      vector[y] = dd;
      d3 = d2;
      d2 = d1;
      d1 = d0;
      d0 = dy;
    }
  }

  for (; x < lb;)
  {
    d0 = x;
    bx0 = (int)b[offset + d0];
    dd = x++;
    for (y = 0; y < len; y = y + 2)
    {
      dy = vector[y];
      vector[y] = dd = _min(dy, d0, dd, bx0, vector[y + 1]);
      d0 = dy;
    }
  }

  return dd;
}

int longest_common_substring(char *a, int la, char *b, int lb)
{
  int table[2][lb + 1];
  memset(table, 0, sizeof table);
  int result = 0;
  for (int i = 1; i <= la; i++)
  {
    for (int j = 1; j <= lb; j++)
    {
      if (a[i - 1] == b[j - 1])
      {
        table[i % 2][j] = table[(i - 1) % 2][j - 1] + 1;
        if (table[i % 2][j] > result)
          result = table[i % 2][j];
      }
      else
        table[i % 2][j] = 0;
    }
  }
  return result;
}

int prefix_match(char *a, int la, char *b, int lb)
{
  int common_prefix = 0;
  while (common_prefix < la && common_prefix < lb && tolower(a[common_prefix]) == tolower(b[common_prefix]))
  {
    common_prefix++;
  }
  return common_prefix;
}

void clean_string(char *s)
// Loop through s with i as the current char to look at, and j as the location to write the next char in
{
  int i, j = 0;
  for (int i = 0; s[i] != '\0'; i++)
  {
    if (s[i] != ',' && s[i] != '.' && s[i] != '!' && s[i] != '?' && s[i] != ':' && s[i] != ';')
    {
      if (s[i] == -61 && s[i + 1] == -87) // é to e
      {
        s[j] = 'e';
      }
      else
      {
        s[j] = tolower(s[i]);
      }
      j++;
    }
  }
  s[j] = '\0';
}

int match_score(char *search_term, char *title)
{
  clean_string(search_term);
  clean_string(title);
  int len_search_term = strlen(search_term);
  int len_title = strlen(title);
  // How many characters to add/fix before the title matches
  int distance = levenshtein(search_term, len_search_term, title, len_title);
  // How many characters to fix if the rest will be typed correctly
  int possible_typos = len_search_term - (len_title - distance);
  float acceptable_typo_fraction = 0.4;
  float typo_fraction = (float)possible_typos / len_search_term;
  int common_length = longest_common_substring(search_term, len_search_term, title, len_title);
  int common_prefix = prefix_match(search_term, len_search_term, title, len_title);
  int score = (len_search_term - possible_typos) + common_length;
  // printf("%s : distance=%d, possible_typos=%d, lcs=%d, common_prefix=%d, score=%d\n", title, distance, possible_typos, common_length, common_prefix, score);
  if ((len_search_term > 0 && common_length == 0) || typo_fraction > acceptable_typo_fraction)
  {
    // To far away, don't bother showing
    return -1;
  }
  return (len_search_term - possible_typos) + common_length + common_prefix / 2 - distance / 4;
}

// int main()
// {
//   char a[] = "A";
//   char b[] = "a";
//   printf("Score: %d\n", match_score(a, b));
// }