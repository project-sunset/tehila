from ctypes import CDLL, c_char_p

match_score = CDLL('./match_score.so')

tests = []

with open('levenshtein_tests.csv', 'r') as f:
    for line in f:
        data = line.split(';')
        tests.append({
            'a': [c_char_p(data[0].encode('utf-8')), len(data[0])],
            'b': [c_char_p(data[1].encode('utf-8')), len(data[1])],
            'result': int(data[2])
        })

for i, test in enumerate(tests):
    actual_result = match_score.levenshtein(test['a'][0], test['a'][1], test['b'][0], test['b'][1])
    assert actual_result == test['result'], f'Test {i+1}: {actual_result} != {test["result"]}'
print('Levenshtein OK')

lcs_tests = [
    ['wop', 'o', 1],
    ['w', 'w', 1],
    ['abc', 'def', 0],
    ['this, is life', 'that, so, life', 5]
]

for i, test in enumerate(lcs_tests):
    a = c_char_p(test[0].encode('utf-8'))
    la = len(test[0])
    b = c_char_p(test[1].encode('utf-8'))
    lb = len(test[1])
    actual_result = match_score.longest_common_substring(a, la, b, lb)
    assert actual_result == test[2], f'LCS {i+1}: {actual_result} != {test[2]}'
print('Longest Common Substring OK')


search_term = c_char_p('hallel'.encode('utf-8'))
song_titles = ['halleluja', 'hallelujah', 'psalm 1', 'bolletje hallal', 'hello', 'the title search']
song_titles.sort(key=lambda title: match_score.match_score(search_term, c_char_p(title.encode('utf-8'))), reverse=True)
print(' | '.join(song_titles))


search_term = c_char_p('adem'.encode('utf-8'))
song_titles = ['adem om van u te zingen', 'Adem', 'Adembenemend', 'Abba, Vader', 'Aanbid hem']
song_titles.sort(key=lambda title: match_score.match_score(search_term, c_char_p(title.encode('utf-8'))), reverse=True)
print(' | '.join(song_titles))

search_term = c_char_p('een'.encode('utf-8'))
song_titles = ['flubber', 'U maakt ons één']
song_titles.sort(key=lambda title: match_score.match_score(search_term, c_char_p(title.encode('utf-8'))), reverse=True)
print(' | '.join(song_titles))