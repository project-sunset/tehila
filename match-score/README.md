The match score is to score how well a search term matches with a title.

It is implemented in C so that it can be used in both the backend and frontend.

Since it is a unique component there is no support in the pipeline to build it, so if it is changed you need to manually run ./build.sh to build a new version. The artifacts should then committed in Git.